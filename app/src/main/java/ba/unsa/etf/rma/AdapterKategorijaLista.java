package ba.unsa.etf.rma;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class AdapterKategorijaLista extends ArrayAdapter<Kategorija> {


    ArrayList<Kategorija> kategorije = new ArrayList<>();

    public AdapterKategorijaLista(Context context, int textViewResourceId, ArrayList<Kategorija> objects) {
        super(context, textViewResourceId, objects);
        kategorije = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.lista_kategorije, null);
        TextView textView = (TextView) v.findViewById(R.id.textView5);
        textView.setText(kategorije.get(position).getNaziv());
        return v;

    }

}
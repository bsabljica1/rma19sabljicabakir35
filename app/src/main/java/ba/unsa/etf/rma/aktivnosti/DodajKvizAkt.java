package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import ba.unsa.etf.rma.AdapterKategorija;
import ba.unsa.etf.rma.AdapterMogucaPitanja;
import ba.unsa.etf.rma.AdapterPitanje;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajKvizAkt extends AppCompatActivity {

    private EditText editText;
    private ListView listView1;
    private ListView listView2;
    private Spinner spinner;
    private Button button;
    private Button importKviz;
    private AdapterKategorija adapterKategorija;
    private AdapterPitanje adapterPitanje;
    private AdapterMogucaPitanja adapterPitanje2;
    private Pitanje dodajPitanje = new Pitanje("Dodaj pitanje","", null,"");
    private ArrayList<Pitanje> listaTrenutnihPitanja = new ArrayList<>();
    private ArrayList<Pitanje> listaMogucihPitanja = new ArrayList<>();
    private ArrayList<Kategorija> listaKategorija = new ArrayList<>();
    private ArrayList<String> dokumentiKategorija = new ArrayList<>();
    private ArrayList<Kviz> kvizovi = new ArrayList<>();
    private Kategorija dodajKategoriju = new Kategorija("Dodaj kategoriju","dodaj");
    private Kviz azuriraniKviz;
    private String nazivKviza = new String();
    private Kviz noviKviz;
    private String dokumentKategorijaId = "";
    private ArrayList<String> listaDokumentPitanja = new ArrayList<>();
    private String dokumentPotrebnogKviza;
    private String potrebnaKategorija;
    private ArrayList<String> potrebnaPitanja = new ArrayList<>();
    private Kategorija pronadjenaKategorija;
    private ArrayList<Pitanje> pitanja = new ArrayList<>();
    private ArrayList<String> dokumentPitanja = new ArrayList<>();
    private ArrayList<String> dokumentKvizova = new ArrayList<>();
    private String azuriranje;
    private String n1;
    private Pitanje novoPitanje;
    private Kategorija nova;
    private Context con = this;
    private ArrayList<Pitanje> pitanjaImporta = new ArrayList<>();
    private boolean pokrenutoDodavanje;
    private String prvobitniNazivKviza;
    private String dokumentRangliste;
    private ArrayList<String> ucitaniIgraciIme = new ArrayList<>();
    private ArrayList<Double> ucitaniIgraciProcenat = new ArrayList<>();









    public class KreirajPitanjeTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            for (int k=0; k<pitanjaImporta.size(); k++) {
                try {
                    InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                    credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                    credentials.refreshToken();
                    String TOKEN = credentials.getAccessToken();

                    String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                    HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");

                    int indexTacnog = -1;
                    for (int i = 0; i < pitanjaImporta.get(k).getOdgovori().size(); i++) {
                        if (pitanjaImporta.get(k).getOdgovori().get(i).equals(pitanjaImporta.get(k).getTacan()))
                            indexTacnog = i;
                    }

                    String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + pitanjaImporta.get(k).getNaziv() + "\"}, \"indexTacnog\": {\"integerValue\":\"" + indexTacnog + "\"}" +
                            ", \"odgovori\": {\"arrayValue\": {\"values\": [";
                    String odgovori = "";
                    for (int i = 0; i < pitanjaImporta.get(k).getOdgovori().size(); i++) {
                        odgovori += "{\"stringValue\": \"" + pitanjaImporta.get(k).getOdgovori().get(i) + "\"}";
                        if (i != pitanjaImporta.get(k).getOdgovori().size() - 1) odgovori += ", ";
                    }
                    dokument += odgovori + "]}} }}";
                    try (OutputStream os = conn.getOutputStream()) {
                        byte[] input = dokument.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }

                    int code = conn.getResponseCode();
                    InputStream odgovor = conn.getInputStream();
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(odgovor, "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listaTrenutnihPitanja=pitanjaImporta;
            listaTrenutnihPitanja.add(dodajPitanje);
            adapterPitanje = new AdapterPitanje(con, R.layout.element_liste, listaTrenutnihPitanja);
            listView1.setAdapter(adapterPitanje);
        }
    }










    public class KreirajKvizTask extends AsyncTask<String,Void,Void> {

    @Override
    protected Void doInBackground(String... strings) {

        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();

            String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kvizovi?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+ noviKviz.getNaziv() +"\"}, \"idKategorije\": {\"stringValue\":\""+
                    dokumentKategorijaId +"\"}" +
                    ", \"pitanja\": {\"arrayValue\": {\"values\": [" ;
                String pitanja="";
                for (int i=0; i<listaDokumentPitanja.size(); i++) {
                    pitanja += "{\"stringValue\": \""+listaDokumentPitanja.get(i)+"\"}";
                    if (i!=listaDokumentPitanja.size()-1) pitanja+=", ";
                }
                dokument+=pitanja+"]}} }}";
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = dokument.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) !=null ) {
                    response.append(responseLine.trim());
                }
            }
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
            DodajKvizAkt.this.startActivity(myIntent);
        }
    }




    public class AzurirajKvizTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kvizovi/"+ dokumentPotrebnogKviza+"?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("PATCH");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+ noviKviz.getNaziv() +"\"}, \"idKategorije\": {\"stringValue\":\""+
                        dokumentKategorijaId +"\"}" +
                        ", \"pitanja\": {\"arrayValue\": {\"values\": [" ;
                String pitanja="";
                for (int i=0; i<listaDokumentPitanja.size(); i++) {
                    pitanja += "{\"stringValue\": \""+listaDokumentPitanja.get(i)+"\"}";
                    if (i!=listaDokumentPitanja.size()-1) pitanja+=", ";
                }
                dokument+=pitanja+"]}} }}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim());
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new ProcitajBazuRangliste().execute("dsd");
          //  Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
           // DodajKvizAkt.this.startActivity(myIntent);
        }
    }













    public class ProcitajBazuRangliste extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            int pomNbr = 1;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i = 0; i < dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivKviza1 = konkretnoPitanje.getJSONObject("nazivKviza");
                            String n = nazivKviza1.getString("stringValue");
                            if (!n.equals(prvobitniNazivKviza)) continue;
                            System.out.println("Postoji rang lista za kviz");
                            dokumentRangliste = dijelovi[6];
                            JSONObject lista = konkretnoPitanje.getJSONObject("lista");
                            JSONObject mapa = lista.getJSONObject("mapValue");
                            JSONObject pozicijeListaDva = mapa.getJSONObject("fields");

                            String pom = Integer.toString(pomNbr);

                            while (pozicijeListaDva.has(pom)) {
                                JSONObject map = pozicijeListaDva.getJSONObject(pom);
                                JSONObject fields = map.getJSONObject("mapValue");
                                String ime = fields.getString("fields");
                                String[] dio = ime.split(":");
                                String name = "";
                                for (int j = 2; j < dio[0].length() - 1; j++) {
                                    name += dio[0].charAt(j);
                                }
                                String broj = "";

                                for (int j = 0; j < dio[2].length(); j++) {
                                    if (dio[2].charAt(j) == '"') {
                                        do {
                                            j++;
                                            if (dio[2].charAt(j) != '"')
                                                broj += dio[2].charAt(j);
                                        } while (dio[2].charAt(j) != '"');
                                    }
                                }
                                broj.trim();


                                Double proc = Double.parseDouble(broj);

                                ucitaniIgraciIme.add(name);
                                ucitaniIgraciProcenat.add(proc);
                                pomNbr += 1;
                                pom = Integer.toString(pomNbr);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (ucitaniIgraciIme.size() > 0) {
                new KreirajRangListTask().execute("dsd");
            }
            else {
                Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                DodajKvizAkt.this.startActivity(myIntent);
            }
        }
    }











    public class KreirajRangListTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                String url ="";
                HttpURLConnection conn;
                    url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste/" + dokumentRangliste + "?access_token=";
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                    conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("PATCH");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\": { \"nazivKviza\": {\"stringValue\":\""+ noviKviz.getNaziv() +"\"}, \"lista\": {\"mapValue\": {\"fields\": {";
                String igraci="";
                for (int i=0; i<ucitaniIgraciIme.size(); i++) {
                    int pozicija = i+1;
                    igraci += "\""+pozicija+"\": {\"mapValue\": {\"fields\": {\""+ucitaniIgraciIme.get(i)+"\": {\"stringValue\":\""+ucitaniIgraciProcenat.get(i).intValue()+"\"}}}}";
                    if (i!=ucitaniIgraciIme.size()-1) igraci+=", ";
                }
                dokument+=igraci+"}}}}}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim());
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
            DodajKvizAkt.this.startActivity(myIntent);
        }
    }







    public class DajDokumentKategorija extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String nazivDokumenta = kata.getString("name");
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            JSONObject idKate = konkretnaKata.getJSONObject("idIkonice");
                            String n = nazivKate.getString("stringValue");
                            String id = idKate.getString("integerValue");
                            dokumentKategorijaId = "Svi";

                            String[] dijelovi = nazivDokumenta.split("/");

                            if (noviKviz.getKategorija().getNaziv().equals(n) && noviKviz.getKategorija().getId().equals(id)) {
                                dokumentKategorijaId = dijelovi[6];
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (azuriranje.equals("ne"))
                new KreirajKvizTask().execute("projekat");
            else if (azuriranje.equals("da"))
                new AzurirajKvizTask().execute("projekat");
        }
    }




    public class DajDokumentPitanja extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String nazivDokumenta = kata.getString("name");
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            String n = nazivKate.getString("stringValue");

                            String[] dijelovi = nazivDokumenta.split("/");
                            for (int j=0; j<noviKviz.getPitanja().size(); j++) {
                                if (noviKviz.getPitanja().get(j).getNaziv().equals(n)) {
                                    listaDokumentPitanja.add(dijelovi[6]);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new DajDokumentKategorija().execute("projekat");
        }
    }






    public class PronadjiPotrebniKviz extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumentKviza = dijelovi[6];
                            JSONObject konkretanKviz = jedno.getJSONObject("fields");
                            JSONObject nazivKvizaa = konkretanKviz.getJSONObject("naziv");
                            JSONObject idKat = konkretanKviz.getJSONObject("idKategorije");
                            JSONObject lPit = konkretanKviz.getJSONObject("pitanja");
                            JSONObject lPitanja = lPit.getJSONObject("arrayValue");
                            ArrayList<String> pitanja = new ArrayList<>();
                            if (lPitanja.has("values")) {
                                JSONArray listaPitanja = lPitanja.getJSONArray("values");


                                for (int j = 0; j < listaPitanja.length(); j++) {
                                    JSONObject jedanOdg = listaPitanja.getJSONObject(j);
                                    String value = jedanOdg.getString("stringValue");
                                    pitanja.add(value); // LISTA ID PITANJA
                                }

                            }

                            n1 = nazivKvizaa.getString("stringValue"); //NAZIV KVIZA
                            String idKatString = idKat.getString("stringValue"); // ID KATEGORIJE

                            if (dokumentPotrebnogKviza.equals(dokumentKviza)) {
                                nazivKviza = n1;
                                prvobitniNazivKviza = n1;
                                potrebnaKategorija = idKatString;
                                potrebnaPitanja = pitanja;
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new PronadjiPotrebnuKategoriju().execute("projekat");
        }
    }






    public class PronadjiPotrebnuKategoriju extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String nazivDokumenta = kata.getString("name");
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            JSONObject idKate = konkretnaKata.getJSONObject("idIkonice");
                            String n = nazivKate.getString("stringValue");
                            String id = idKate.getString("integerValue");

                            String[] dijelovi = nazivDokumenta.split("/");

                            if (potrebnaKategorija.equals(dijelovi[6])) {
                                pronadjenaKategorija = new Kategorija(n, id);
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            new PronadjiPotrebnaPitanja().execute("projekat");

        }
    }



    public class PronadjiPotrebnaPitanja extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumenPitanja = dijelovi[6];
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivPitanja = konkretnoPitanje.getJSONObject("naziv");
                            JSONObject indexTacnogPitanja = konkretnoPitanje.getJSONObject("indexTacnog");
                            JSONObject lOdg = konkretnoPitanje.getJSONObject("odgovori");
                            JSONObject lOdgovora = lOdg.getJSONObject("arrayValue");
                            JSONArray listaOdgovora = lOdgovora.getJSONArray("values");

                            ArrayList<String> odgovori = new ArrayList<>();
                            for (int j=0; j<listaOdgovora.length(); j++) {
                                JSONObject jedanOdg = listaOdgovora.getJSONObject(j);
                                String value = jedanOdg.getString("stringValue");
                                odgovori.add(value); //LISTA ODGOVORA
                            }

                            String n = nazivPitanja.getString("stringValue"); //NAZIV PITANJA
                            String index = indexTacnogPitanja.getString("integerValue"); //INDEX TACNOG

                            if (potrebnaPitanja.size()!=0) {

                                boolean pronadjenoPitanje = false;
                                for (int j = 0; j < potrebnaPitanja.size(); j++) {
                                    if (potrebnaPitanja.get(j).equals(dokumenPitanja)) {
                                        listaTrenutnihPitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));
                                        pronadjenoPitanje = true;
                                        break;
                                    }
                                }

                                if (!pronadjenoPitanje)
                                    listaMogucihPitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));

                            }

                            else
                                listaMogucihPitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            azuriraniKviz = (Kviz) getIntent().getSerializableExtra("kviz");

            if (azuriraniKviz != null) {

                nazivKviza = azuriraniKviz.getNaziv();
                listaTrenutnihPitanja = azuriraniKviz.getPitanja();
                pronadjenaKategorija = azuriraniKviz.getKategorija();


                for (int i = 0; i < listaTrenutnihPitanja.size(); i++) {
                    for (int j = 0; j < listaMogucihPitanja.size(); j++) {
                        if (listaTrenutnihPitanja.get(i).getNaziv().equals(listaMogucihPitanja.get(j).getNaziv()))
                            listaMogucihPitanja.remove(listaMogucihPitanja.get(j));
                    }
                }

            }


            azuriranjePitanja();
            azuriranjeKategorije();

            adapterPitanje2 = new AdapterMogucaPitanja(con, R.layout.element_liste, listaMogucihPitanja);


            if ((!nazivKviza.equals("")) && (!nazivKviza.equals("-1")))
                editText.setText(nazivKviza);

            listView2.setAdapter(adapterPitanje2);
        }
    }



















    public class KreirajKategorijuTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+ nova.getNaziv() +"\"}, \"idIkonice\": {\"integerValue\":\""+
                        nova.getId() +"\"} }}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listaKategorija.remove(dodajKategoriju);
            listaKategorija.add(nova);
            listaKategorija.add(dodajKategoriju);
            adapterKategorija = new AdapterKategorija(con, R.layout.support_simple_spinner_dropdown_item, listaKategorija);
            spinner.setAdapter(adapterKategorija);
            spinner.setSelection(listaKategorija.size()-2);
        }
    }














    public class ProcitajBazuKategorija extends AsyncTask<String, Void, Void> {

        private Context con;

        public ProcitajBazuKategorija(Context c) {
            con = c;
        }

        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String str = kata.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumentId = dijelovi[6];
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            JSONObject idKate = konkretnaKata.getJSONObject("idIkonice");
                            String n = nazivKate.getString("stringValue");
                            String id = idKate.getString("integerValue");
                            boolean postojiKategorija = false;
                            for (int j=0; j<listaKategorija.size(); j++) {
                                if (listaKategorija.get(j).getNaziv().equals(n) || listaKategorija.get(j).getId().equals(id))
                                    postojiKategorija = true;
                            }
                            if (!postojiKategorija) {
                                listaKategorija.add(new Kategorija(n, id));
                                dokumentiKategorija.add(dokumentId);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            new ProcitajBazuPitanja(con).execute("dsd");

        }
    }




    public class ProcitajBazuPitanja extends AsyncTask<String, Void, Void> {

        private Context con;

        public ProcitajBazuPitanja(Context c) {
            con = c;
        }

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumenPitanja = dijelovi[6];
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivPitanja = konkretnoPitanje.getJSONObject("naziv");
                            JSONObject indexTacnogPitanja = konkretnoPitanje.getJSONObject("indexTacnog");
                            JSONObject lOdg = konkretnoPitanje.getJSONObject("odgovori");
                            JSONObject lOdgovora = lOdg.getJSONObject("arrayValue");
                            JSONArray listaOdgovora = lOdgovora.getJSONArray("values");

                            ArrayList<String> odgovori = new ArrayList<>();
                            for (int j=0; j<listaOdgovora.length(); j++) {
                                JSONObject jedanOdg = listaOdgovora.getJSONObject(j);
                                String value = jedanOdg.getString("stringValue");
                                odgovori.add(value);
                            }

                            String n = nazivPitanja.getString("stringValue");
                            String index = indexTacnogPitanja.getString("integerValue");
                            boolean postojiPitanje = false;
                            for (int j=0; j<pitanja.size(); j++) {
                                if (pitanja.get(j).getNaziv().equals(n))
                                    postojiPitanje = true;
                            }


                            if (!postojiPitanje) {
                                pitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));
                                dokumentPitanja.add(dokumenPitanja);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            new ProcitajBazuKvizova(con, new Kategorija("Svi","svi")).execute("dsd");

        }
    }








    public class ProcitajBazuKvizova extends AsyncTask<String, Void, Void> {



        private Context con;
        private Kategorija trenutnaKategorija;

        public ProcitajBazuKvizova(Context c, Kategorija k) {
            con = c;
            trenutnaKategorija = k;
        }



        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumentKviza = dijelovi[6];
                            JSONObject konkretanKviz = jedno.getJSONObject("fields");
                            JSONObject nazivKviza = konkretanKviz.getJSONObject("naziv");
                            JSONObject idKat = konkretanKviz.getJSONObject("idKategorije");
                            JSONObject lPit = konkretanKviz.getJSONObject("pitanja");
                            JSONObject lPitanja = lPit.getJSONObject("arrayValue");
                            ArrayList<String> pitanjaS = new ArrayList<>();
                            if (lPitanja.has("values")) {
                                JSONArray listaPitanja = lPitanja.getJSONArray("values");


                                for (int j = 0; j < listaPitanja.length(); j++) {
                                    JSONObject jedanOdg = listaPitanja.getJSONObject(j);
                                    String value = jedanOdg.getString("stringValue");
                                    pitanjaS.add(value); // LISTA ID PITANJA
                                }

                            }

                            String n = nazivKviza.getString("stringValue"); //NAZIV KVIZA
                            String idKatString = idKat.getString("stringValue"); // ID KATEGORIJE

                            ArrayList<Pitanje> potrebnaPitanja1 = new ArrayList<>();

                            for (int j=0; j<pitanjaS.size(); j++) {
                                for (int k=0; k<pitanja.size(); k++) {
                                    if (pitanja.get(j).equals(dokumentPitanja.get(k)))
                                        potrebnaPitanja1.add(pitanja.get(k));
                                }
                            }

                            Kategorija potrebnaKategorija1 = new Kategorija("Svi","svi");

                            for (int j=0; j<dokumentiKategorija.size(); j++) {
                                if (dokumentiKategorija.get(j).equals(idKatString))
                                    potrebnaKategorija1 = listaKategorija.get(j+1);
                            }



                            boolean postojiKviz = false;
                            for (int j=0; j<kvizovi.size(); j++) {
                                if (kvizovi.get(j).getNaziv().equals(n))
                                    postojiKviz = true;
                            }
                            if (!postojiKviz) {
                                if (trenutnaKategorija.getNaziv().equals("Svi")) {
                                    kvizovi.add(new Kviz(n, potrebnaPitanja1, potrebnaKategorija1));
                                    dokumentKvizova.add(dokumentKviza);
                                }
                                else {
                                    if (trenutnaKategorija.getNaziv().equals(potrebnaKategorija1.getNaziv()) && trenutnaKategorija.getId().equals(potrebnaKategorija1.getId())) {
                                        kvizovi.add(new Kviz(n, potrebnaPitanja1, potrebnaKategorija1));
                                        dokumentKvizova.add(dokumentKviza);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            kvizovi.clear();
            dokumentKvizova.clear();

        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (pokrenutoDodavanje) {
                if (!validacijaNazivaKviza()) return;
                listaTrenutnihPitanja.remove(dodajPitanje);
                noviKviz = new Kviz(editText.getText().toString(), listaTrenutnihPitanja,(Kategorija) spinner.getSelectedItem());
                new DajDokumentPitanja().execute("projekat");
            }

            else {

                dokumentPotrebnogKviza = getIntent().getStringExtra("dokumentKviza");
                azuriranje = getIntent().getStringExtra("azuriranje");
                nazivKviza = "";

                if (dokumentPotrebnogKviza != null) {
                    new PronadjiPotrebniKviz().execute("projekat");
                } else {
                    new PronadjiPotrebnaPitanja().execute("projekat");
                }

            }

        }
    }


























    private void azuriranjePitanja () {

        listaTrenutnihPitanja.add(dodajPitanje);
        adapterPitanje = new AdapterPitanje(this, R.layout.element_liste, listaTrenutnihPitanja);
        listView1.setAdapter(adapterPitanje);

    }

    private void azuriranjeKategorije () {

            listaKategorija.add(dodajKategoriju);
            adapterKategorija = new AdapterKategorija(this, R.layout.support_simple_spinner_dropdown_item, listaKategorija);
            spinner.setAdapter(adapterKategorija);


            if (pronadjenaKategorija != null) {
                for (int i = 0; i < listaKategorija.size(); i++)
                    if (listaKategorija.get(i).getId().equals(pronadjenaKategorija.getId()))
                        spinner.setSelection(i);
            } else {
                spinner.setSelection(0);
            }
        }


    private boolean validacijaNazivaKviza () {

        if (editText.length()==0) {
            editText.setBackgroundColor(Color.parseColor("#ffaaaa"));
            return false;
        }

        if (azuriranje.equals("da")) {
            if (editText.getText().toString().equals(n1)) return true;
            for (int i = 0; i < kvizovi.size(); i++) {
                if (kvizovi.get(i).getNaziv().equals(n1)) continue;
                if (kvizovi.get(i).getNaziv().equals(editText.getText().toString())) {
                    editText.setBackgroundColor(Color.parseColor("#ffaaaa"));
                    return false;
                }
            }
        }

        else if (azuriranje.equals("ne")) {
            for (int i = 0; i < kvizovi.size(); i++) {
                if (kvizovi.get(i).getNaziv().equals(editText.getText().toString())) {
                    editText.setBackgroundColor(Color.parseColor("#ffaaaa"));
                    return false;
                }
            }
        }

        return true;

    }


    private void bojaPozadine(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                return;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void afterTextChanged(Editable s) {
                return;
            }
        });
    }



    public boolean ProvjeraKonekcije() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else
            connected = false;

        return connected;
    }






    //ON CREATE METODA

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_kviz_akt);
        editText = (EditText) findViewById(R.id.etNaziv);
        listView2 = (ListView) findViewById(R.id.lvMogucaPitanja);
        button = (Button) findViewById(R.id.btnDodajKviz);
        importKviz = (Button) findViewById(R.id.btnImportKviz);
        listView1 = (ListView) findViewById(R.id.lvDodanaPitanja);
        spinner = (Spinner) findViewById(R.id.spKategorije);
        listaKategorija.clear();
        listaKategorija.add(new Kategorija("Svi","svi"));
        pokrenutoDodavanje = false;

        if (!ProvjeraKonekcije()) {
            Toast.makeText(DodajKvizAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
            DodajKvizAkt.this.startActivity(myIntent);
        }


            new ProcitajBazuKategorija(this).execute("dsd");



        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKvizAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                    DodajKvizAkt.this.startActivity(myIntent);
                }

                Pitanje izabranoPitanje = adapterPitanje.getItem(position);
                if (izabranoPitanje!=dodajPitanje) {

                    listaTrenutnihPitanja.remove(izabranoPitanje);
                    listaMogucihPitanja.add(izabranoPitanje);

                    adapterPitanje.notifyDataSetChanged();
                    adapterPitanje2.notifyDataSetChanged();
                }
                else if (izabranoPitanje.getNaziv().equals(dodajPitanje.getNaziv())) {
                    Intent aktivnostDodajPitanje = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
                    listaTrenutnihPitanja.remove(dodajPitanje);
                    azuriraniKviz = new Kviz(editText.getText().toString(), listaTrenutnihPitanja,(Kategorija) spinner.getSelectedItem());
                        aktivnostDodajPitanje.putExtra("kviz", azuriraniKviz);
                        aktivnostDodajPitanje.putExtra("dokumentKviza", dokumentPotrebnogKviza);
                        aktivnostDodajPitanje.putExtra("azuriranje", azuriranje);

                    DodajKvizAkt.this.startActivity(aktivnostDodajPitanje);
                }

            }});


        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKvizAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                    DodajKvizAkt.this.startActivity(myIntent);
                }

                Pitanje izabranoPitanje = adapterPitanje2.getItem(position);
                listaMogucihPitanja.remove(izabranoPitanje);
                Pitanje dodajPitanje = listaTrenutnihPitanja.get(listaTrenutnihPitanja.size()-1);
                listaTrenutnihPitanja.remove(dodajPitanje);
                listaTrenutnihPitanja.add(izabranoPitanje);
                listaTrenutnihPitanja.add(dodajPitanje);

                adapterPitanje.notifyDataSetChanged();
                adapterPitanje2.notifyDataSetChanged();

            }});


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKvizAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                    DodajKvizAkt.this.startActivity(myIntent);
                }

                pokrenutoDodavanje = true;
                new ProcitajBazuKategorija(con).execute("dsd");
                Toast.makeText(DodajKvizAkt.this, "Validacija i dodavanje kviza u toku", Toast.LENGTH_LONG).show();
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKvizAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                    DodajKvizAkt.this.startActivity(myIntent);
                }

                if (position == spinner.getCount()-1) {

                    Intent aktivnostDodajKategoriju = new Intent(DodajKvizAkt.this, DodajKategorijuAkt.class);
                        listaTrenutnihPitanja.remove(dodajPitanje);
                        azuriraniKviz = new Kviz(editText.getText().toString(), listaTrenutnihPitanja, new Kategorija("Svi", "svi"));
                        aktivnostDodajKategoriju.putExtra("kviz", azuriraniKviz);
                        aktivnostDodajKategoriju.putExtra("dokumentKviza", dokumentPotrebnogKviza);
                        aktivnostDodajKategoriju.putExtra("azuriranje", azuriranje);

                    DodajKvizAkt.this.startActivity(aktivnostDodajKategoriju);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bojaPozadine(editText);


        importKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKvizAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                    DodajKvizAkt.this.startActivity(myIntent);
                }

                if (azuriranje.equals("da")) return;
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                sendIntent.setType("text/plain");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(sendIntent,0); }
            }
        });


    }



    //CITANJE KVIZA IZ TXT FAJLA

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==0) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri;
                if (data != null) {
                    uri = data.getData();
                    try {
                        InputStream inputStream = null;
                        inputStream = getContentResolver().openInputStream(uri);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                inputStream));
                        String line;
                        String kviz = new String("");
                        do {
                            line = reader.readLine();
                            if (line == null) break;
                            kviz = kviz + line + "\n";
                        } while (true);
                        if (inputStream!=null)
                        inputStream.close();
                        ImplementirajKviz(kviz);
                    } catch (Exception e){}
                }
                }
        }
    }


    //IMPLEMENTACIJA KVIZA IZ TXT FAJLA

    private void ImplementirajKviz (String kviz) {
        if (kviz==null) return;
        String[] prvaPodjela = kviz.split("\n");
        String[] drugaPodjela = prvaPodjela[0].split(",");


        for (Kviz kvizi : kvizovi) {
            if (kvizi.getNaziv().equals(drugaPodjela[0])) {
                AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                alertDialog.setTitle("Greska");
                alertDialog.setMessage("Kviz kojeg importujete već postoji!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return;
            }
        }


        if ((prvaPodjela.length-1)!=Integer.parseInt(drugaPodjela[2])) {
            AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
            alertDialog.setTitle("Greska");
            alertDialog.setMessage("Kviz kojeg imporujete ima neispravan broj pitanja!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }


        pitanjaImporta = new ArrayList<>();
        ArrayList<String> naziviPitanja = new ArrayList<>();
        for (int i=1; i<prvaPodjela.length; i++) {
            String[] pitanje = prvaPodjela[i].split(",");
            String nazivTekstPitanja = pitanje[0];
            if (naziviPitanja.contains(nazivTekstPitanja)) {
                AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                alertDialog.setTitle("Greska");
                alertDialog.setMessage("Kviz kojeg importujete nije ispravan jer postoji ponavljanje pitanja!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return;
            }
            naziviPitanja.add(nazivTekstPitanja);
            int brojOdgovora = Integer.parseInt(pitanje[1]);
            int tacanOdgovor = Integer.parseInt(pitanje[2]);

            if (brojOdgovora!= pitanje.length-3) {
                AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                alertDialog.setTitle("Greska");
                alertDialog.setMessage("Kviz kojeg importujete ima neispravan broj odgovora!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return;
            }

            if (tacanOdgovor<0 || tacanOdgovor>pitanje.length-4) {
                AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                alertDialog.setTitle("Greska");
                alertDialog.setMessage("Kviz kojeg importujete ima neispravan index tačnog odgovora!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return;
            }

            ArrayList<String> odgovori = new ArrayList<>();
            int brojac=0;
            while (brojac!=brojOdgovora) {
                if (odgovori.contains(pitanje[2+brojac+1])) {
                    AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                    alertDialog.setTitle("Greska");
                    alertDialog.setMessage("Kviz kojeg importujete nije ispravan jer postoji ponavljanje odgovora!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }
                odgovori.add(pitanje[2+brojac+1]);
                brojac++;
            }


            novoPitanje = new Pitanje(nazivTekstPitanja,nazivTekstPitanja,odgovori,odgovori.get(tacanOdgovor));
            for (int j=0; j<listaMogucihPitanja.size(); j++) {
                if (listaMogucihPitanja.get(j).getNaziv().equals(nazivTekstPitanja)) {
                    AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                    alertDialog.setTitle("Greska");
                    alertDialog.setMessage("Jedno od pitanja koje zelite importovati vec postoji u bazi!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }
            }
            for (int j=0; j<listaTrenutnihPitanja.size(); j++) {
                if (listaTrenutnihPitanja.get(j).getNaziv().equals(nazivTekstPitanja)) {
                    AlertDialog alertDialog = new AlertDialog.Builder(DodajKvizAkt.this).create();
                    alertDialog.setTitle("Greska");
                    alertDialog.setMessage("Jedno od pitanja koje zelite importovati vec postoji u bazi!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }
            }
            pitanjaImporta.add(novoPitanje);
        }

        new KreirajPitanjeTask().execute("dsd");


        editText.setText(drugaPodjela[0]);

        boolean novaKategorija = true;
        for (int i=0; i<listaKategorija.size(); i++) {
            if (listaKategorija.get(i).getNaziv().equals(drugaPodjela[1])) {
                spinner.setSelection(i);
                novaKategorija = false;
            }
        }
        if (novaKategorija) {
            Random rand = new Random();
            boolean slobodanId = true;
            String noviId= new String();
            do {
                noviId = Integer.toString(rand.nextInt(101));
                for (Kategorija kate : listaKategorija)
                    if (kate.getId().equals(noviId)) slobodanId=false;
            } while (!slobodanId);

            nova = new Kategorija(drugaPodjela[1],noviId);

                new KreirajKategorijuTask().execute("dsd");

        }

    }


    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
        DodajKvizAkt.this.startActivity(myIntent);
    }
}


package ba.unsa.etf.rma;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.Kategorija;

public class AdapterKategorija extends ArrayAdapter<Kategorija> {


    ArrayList<Kategorija> kategorije = new ArrayList<>();


    public AdapterKategorija(Context context, int textViewResourceId, ArrayList<Kategorija> objects) {
        super(context, textViewResourceId, objects);
        kategorije = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(kategorije.get(position).getNaziv());
        return label;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(kategorije.get(position).getNaziv());

        return label;
    }
}
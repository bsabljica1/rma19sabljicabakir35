package ba.unsa.etf.rma;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class AdapterMogucaPitanja extends ArrayAdapter<Pitanje> {


    ArrayList<Pitanje> pitanja = new ArrayList<>();

    public AdapterMogucaPitanja(Context context, int textViewResourceId, ArrayList<Pitanje> objects) {
        super(context, textViewResourceId, objects);
        pitanja = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.element_liste, null);
        ImageView imageView = (ImageView) v.findViewById(R.id.slike);
        TextView textView = (TextView) v.findViewById(R.id.textView);
        textView.setText("Moguce "+pitanja.get(position).getNaziv());
        imageView.setImageResource(R.drawable.dodajdugme);
        return v;

    }

}

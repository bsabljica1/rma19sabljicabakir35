package ba.unsa.etf.rma;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.Kviz;

public class AdapterKviz extends ArrayAdapter<Kviz> {

        private ArrayList<Kviz> kvizovi = new ArrayList<>();

    public AdapterKviz(Context context, int textViewResourceId, ArrayList<Kviz> objects) {
            super(context, textViewResourceId, objects);
            kvizovi = objects;
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.ikone, null);
            IconView iconView = (IconView) v.findViewById(R.id.slike);
            TextView textView = (TextView) v.findViewById(R.id.textView);
            textView.setText(kvizovi.get(position).getNaziv());

             if (kvizovi.get(position).getNaziv().equals("Dodaj kviz")) {
                v = inflater.inflate(R.layout.element_liste, null);
                ImageView imageView = v.findViewById(R.id.slike);
                textView = (TextView) v.findViewById(R.id.textView);
                textView.setText(kvizovi.get(position).getNaziv());
                imageView.setImageResource(R.drawable.dodajdugme);
            }

            else {
                 if (kvizovi.get(position).getKategorija().getId().equals("svi")) {
                     v = inflater.inflate(R.layout.element_liste, null);
                     ImageView imageView = v.findViewById(R.id.slike);
                     textView = (TextView) v.findViewById(R.id.textView);
                     textView.setText(kvizovi.get(position).getNaziv());
                     imageView.setImageResource(R.drawable.crnodugme);
                 }
                 else
                 iconView.setIcon(Integer.parseInt(kvizovi.get(position).getKategorija().getId()));
            }

            return v;

        }

}

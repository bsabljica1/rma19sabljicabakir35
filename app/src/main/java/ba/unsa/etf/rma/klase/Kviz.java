package ba.unsa.etf.rma.klase;

import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Kviz implements Serializable {

    private String naziv;
    private ArrayList<Pitanje> pitanja;
    private Kategorija kategorija;

    public Kviz (String naziv, ArrayList<Pitanje> pitanja, Kategorija kategorija) {
        this.naziv=naziv;
        this.pitanja=pitanja;
        this.kategorija=kategorija;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public ArrayList<Pitanje> getPitanja() {
        return pitanja;
    }

    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }

    public void setPitanja(ArrayList<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }

    public void dodajPitanje(Pitanje pitanje) {
        pitanja.add(pitanje);
    }
}

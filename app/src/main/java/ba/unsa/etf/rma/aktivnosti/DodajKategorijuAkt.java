package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback {

    private EditText nazivKategorije;
    private EditText nazivIkone;
    private Button dodajIkonu;
    private Button dodajKategoriju;
    private ArrayList<Kategorija> kategorije = new ArrayList<>();
    private Kviz kviz;
    private Icon[] ikone;
    private Kategorija novaKategorija;
    private ArrayList<String> dokumentiKategorija = new ArrayList<>();
    private Context con = this;




    public class ProcitajBazuKategorija extends AsyncTask<String, Void, Void> {

        private Context con;

        public ProcitajBazuKategorija(Context c) {
            con = c;
        }

        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String str = kata.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumentId = dijelovi[6];
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            JSONObject idKate = konkretnaKata.getJSONObject("idIkonice");
                            String n = nazivKate.getString("stringValue");
                            String id = idKate.getString("integerValue");
                            boolean postojiKategorija = false;
                            for (int j=0; j<kategorije.size(); j++) {
                                if (kategorije.get(j).getNaziv().equals(n) || kategorije.get(j).getId().equals(id))
                                    postojiKategorija = true;
                            }
                            if (!postojiKategorija) {
                                kategorije.add(new Kategorija(n, id));
                                dokumentiKategorija.add(dokumentId);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            boolean validnaKategorija = false;
            if (validacijaNazivaKategorije())
                if (validacijaNazivaIkone())
                    validnaKategorija=true;

            if (validnaKategorija) {

                //Intent aktivnostDodajKviz = new Intent(DodajKategorijuAkt.this, DodajKvizAkt.class);
                novaKategorija = new Kategorija(nazivKategorije.getText().toString(), nazivIkone.getText().toString());
                kviz = (Kviz) getIntent().getSerializableExtra("kviz");
                kviz.setKategorija(novaKategorija);
                //aktivnostDodajKviz.putExtra("kviz",kviz);
                //aktivnostDodajKviz.putExtra("dokumentKviza", getIntent().getStringExtra("dokumentKviza"));
                //aktivnostDodajKviz.putExtra("azuriranje", getIntent().getStringExtra("azuriranje"));
                new KreirajKategorijuTask().execute("projekat");
                // DodajKategorijuAkt.this.startActivity(aktivnostDodajKviz);

            }
            else {
                AlertDialog alertDialog = new AlertDialog.Builder(DodajKategorijuAkt.this).create();
                alertDialog.setTitle("Greska");
                alertDialog.setMessage("Unesena kategorija vec postoji!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return;
            }

        }
    }





    public class KreirajKategorijuTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");



                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+ novaKategorija.getNaziv() +"\"}, \"idIkonice\": {\"integerValue\":\""+
                        novaKategorija.getId() +"\"} }}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent aktivnostDodajKviz = new Intent(DodajKategorijuAkt.this, DodajKvizAkt.class);
            aktivnostDodajKviz.putExtra("kviz",kviz);
            aktivnostDodajKviz.putExtra("dokumentKviza", getIntent().getStringExtra("dokumentKviza"));
            aktivnostDodajKviz.putExtra("azuriranje", getIntent().getStringExtra("azuriranje"));
            DodajKategorijuAkt.this.startActivity(aktivnostDodajKviz);
        }
    }










    private boolean validacijaNazivaKategorije () {

        if (nazivKategorije.length()==0) {
            nazivKategorije.setBackgroundColor(Color.parseColor("#ffaaaa"));
            return false;
        }
        for (int i=0; i<kategorije.size(); i++)
            if (kategorije.get(i).getNaziv().equals(nazivKategorije.getText().toString())) {
                nazivKategorije.setBackgroundColor(Color.parseColor("#ffaaaa"));
                return false;
            }
        return true;

    }

    private boolean validacijaNazivaIkone () {

        if (nazivIkone.length()==0 || ikone==null || ikone.length==0) {
            nazivIkone.setBackgroundColor(Color.parseColor("#ffaaaa"));
            return false;
        }
        for (int i=0; i<kategorije.size(); i++)
            if (kategorije.get(i).getId().equals(nazivIkone.getText().toString())) {
                nazivIkone.setBackgroundColor(Color.parseColor("#ffaaaa"));
                return false;
            }
        return true;
    }


    private void bojaPozadine(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                return;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void afterTextChanged(Editable s) {
                return;
            }
        });
    }

    public boolean ProvjeraKonekcije() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else
            connected = false;

        return connected;
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_kategoriju_akt);

        nazivKategorije = findViewById(R.id.etNaziv);
        nazivIkone = findViewById(R.id.etIkona);
        dodajIkonu = findViewById(R.id.btnDodajIkonu);
        dodajKategoriju = findViewById(R.id.btnDodajKategoriju);
        con = this;

        if (!ProvjeraKonekcije()) {
            Toast.makeText(DodajKategorijuAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(DodajKategorijuAkt.this, KvizoviAkt.class);
            DodajKategorijuAkt.this.startActivity(myIntent);
        }


        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKategorijuAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKategorijuAkt.this, KvizoviAkt.class);
                    DodajKategorijuAkt.this.startActivity(myIntent);
                }

                    new ProcitajBazuKategorija(con).execute("dsd");
                Toast.makeText(DodajKategorijuAkt.this, "Validacija i dodavanje kategorije u toku", Toast.LENGTH_LONG).show();

            }
        });


        dodajIkonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajKategorijuAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajKategorijuAkt.this, KvizoviAkt.class);
                    DodajKategorijuAkt.this.startActivity(myIntent);
                }

                final IconDialog iconDialog = new IconDialog();
                bojaPozadine(nazivIkone);
                iconDialog.setMaxSelection(1, false);
                iconDialog.setSelectedIcons(ikone);
                iconDialog.show(getSupportFragmentManager(), "icon_dialog");
            }
        });


        bojaPozadine(nazivKategorije);
        bojaPozadine(nazivIkone);



    }



    @Override
    public void onIconDialogIconsSelected(Icon[] icons) {
        ikone = icons;
        nazivIkone.setText("" + ikone[0].getId());
    }



}

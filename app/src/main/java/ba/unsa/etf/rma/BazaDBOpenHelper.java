package ba.unsa.etf.rma;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.ScaleGestureDetector;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class BazaDBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final int DATABASE_VERSION = 1;
    public static final String KEY_ID = "_id";

    public static final String KVIZOVI_TABLE = "Kvizovi";
    public static final String KVIZ_DOKUMENT = "kvizDokument";
    public static final String ID_KATEGORIJE = "idKategorije";
    public static final String KVIZOVI_NAZIV = "naziv";

    public static final String KATEGORIJE_TABLE = "Kategorije";
    public static final String KATEGORIJA_DOKUMENT = "kategorijaDokument";
    public static final String KATEGORIJE_NAZIV = "naziv";
    public static final String ID_IKONICE = "idIkonice";

    public static final String PITANJA_TABLE = "Pitanja";
    public static final String PITANJE_DOKUMENT = "pitanjeDokument";
    public static final String PITANJA_NAZIV = "naziv";
    public static final String ID_KVIZA = "idKviza";

    public static final String ODGOVORI_TABLE = "Odgovori";
    public static final String TEKST_ODGOVORA = "tekstOdgovora";
    public static final String ID_PITANJA = "idPitanja";
    public static final String TACAN = "tacan";

    public static final String RANGLISTE_TABLE = "Rangliste";
    public static final String RANGLISTA_DOKUMENT = "ranglistaDokument";

    public static final String KONKRETNA_LISTA_TABLE = "KonkretnaLista";
    public static final String IME_IGRACA = "imeIgraca";
    public static final String PROCENAT_TACNIH = "procenatTacnih";
    public static final String ID_RANGLISTE = "idRangliste";





    private static final String CREATE_KATEGORIJE = "create table " +
            KATEGORIJE_TABLE + "(" + KEY_ID +
            " integer primary key autoincrement, " + KATEGORIJA_DOKUMENT + " text, "  + KATEGORIJE_NAZIV + " text, " + ID_IKONICE + " text);";

    private static final String CREATE_KVIZOVI = "create table " +
            KVIZOVI_TABLE + "(" + KEY_ID +
            " integer primary key autoincrement, " + KVIZ_DOKUMENT + " text, " + ID_KATEGORIJE + " text, "
            + KVIZOVI_NAZIV  + " text, " + "foreign key ("+ ID_KATEGORIJE +") references "+ KATEGORIJE_TABLE + "("+ KATEGORIJA_DOKUMENT +"));";


    private static final String CREATE_PITANJA = "create table " +
            PITANJA_TABLE + "(" + KEY_ID +
            " integer primary key autoincrement, " + PITANJE_DOKUMENT + " text, " + PITANJA_NAZIV + " text, " + ID_KVIZA + " text, " +
            "constraint fk_kviz foreign key ("+ ID_KVIZA +") references "+ KVIZOVI_TABLE + "("+ KVIZ_DOKUMENT +"));";

    private static final String CREATE_ODGOVORI = "create table " + ODGOVORI_TABLE + "(" + KEY_ID + " integer primary key autoincrement, " + TEKST_ODGOVORA + " text, " +
            TACAN + " integer, " + ID_PITANJA + " text, " + "constraint fk_pitanje foreign key ("+ ID_PITANJA +") references "+ PITANJA_TABLE + "("+ PITANJE_DOKUMENT +"));";

    private static final String CREATE_RANGLISTE = "create table " +
            RANGLISTE_TABLE + "(" + KEY_ID +
            " integer primary key autoincrement, " + RANGLISTA_DOKUMENT + " text, " + ID_KVIZA + " text, " +
            "constraint fk_kviz foreign key ("+ ID_KVIZA +") references "+ KVIZOVI_TABLE + "("+ KVIZOVI_NAZIV +"));";

    private static final String CREATE_KONKRETNALISTA = "create table " + KONKRETNA_LISTA_TABLE + "(" + KEY_ID + " integer primary key autoincrement, " +
            IME_IGRACA + " text, " + PROCENAT_TACNIH + " integer, " + ID_RANGLISTE + " text, " +
            "constraint fk_rangLista foreign key ("+ ID_RANGLISTE +") references "+ RANGLISTE_TABLE + "("+ RANGLISTA_DOKUMENT +"));";





    public BazaDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_KATEGORIJE);
        db.execSQL(CREATE_KVIZOVI);
        db.execSQL(CREATE_PITANJA);
        db.execSQL(CREATE_ODGOVORI);
        db.execSQL(CREATE_RANGLISTE);
        db.execSQL(CREATE_KONKRETNALISTA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + KONKRETNA_LISTA_TABLE + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + RANGLISTE_TABLE + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + ODGOVORI_TABLE + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + PITANJA_TABLE + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + KVIZOVI_TABLE + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + KATEGORIJE_TABLE + "'");
        onCreate(db);
    }

    public void UnosKategorija (ArrayList<Kategorija> kategorije, ArrayList<String> dokumentiKategorija) {

        SQLiteDatabase db = this.getWritableDatabase();

        for (int i=0; i<kategorije.size(); i++) {

            ContentValues vrijednosti = new ContentValues();
            vrijednosti.put(KATEGORIJA_DOKUMENT, dokumentiKategorija.get(i));
            vrijednosti.put(KATEGORIJE_NAZIV, kategorije.get(i).getNaziv());
            vrijednosti.put(ID_IKONICE, kategorije.get(i).getId());
            db.insert(KATEGORIJE_TABLE, null, vrijednosti);

        }
    }

    public void UnosKvizova (ArrayList<Kviz> kvizovi, ArrayList<String> dokumentiKvizova, ArrayList<Kategorija> kategorije, ArrayList<String> dokumentiKategorija) {

        SQLiteDatabase db = this.getWritableDatabase();

        for (int i=0; i<kvizovi.size(); i++) {

            ContentValues vrijednosti = new ContentValues();
            vrijednosti.put(KVIZ_DOKUMENT, dokumentiKvizova.get(i));
            vrijednosti.put(KVIZOVI_NAZIV, kvizovi.get(i).getNaziv());
            for (int j=0; j<kategorije.size(); j++)
                if (kvizovi.get(i).getKategorija().getNaziv().equals(kategorije.get(j).getNaziv()))
            vrijednosti.put(ID_KATEGORIJE, dokumentiKategorija.get(j));
            db.insert(KVIZOVI_TABLE, null, vrijednosti);

        }

    }

    public void UnosPitanja (ArrayList<Pitanje> pitanja, ArrayList<String> dokumentiPitanja, ArrayList<Kviz> kvizovi, ArrayList<String> dokumentiKvizova) {

        SQLiteDatabase db = this.getWritableDatabase();

        for (int i=0; i<kvizovi.size(); i++) {
            for (int j = 0; j < kvizovi.get(i).getPitanja().size(); j++) {

                ContentValues vrijednosti = new ContentValues();
                vrijednosti.put(ID_KVIZA, dokumentiKvizova.get(i));
                vrijednosti.put(PITANJA_NAZIV, kvizovi.get(i).getPitanja().get(j).getNaziv());
                for (int k = 0; k < pitanja.size(); k++) {
                    if (kvizovi.get(i).getPitanja().get(j).getNaziv().equals(pitanja.get(k).getNaziv())) {
                        vrijednosti.put(PITANJE_DOKUMENT, dokumentiPitanja.get(k));
                    }
                }
                db.insert(PITANJA_TABLE, null, vrijednosti);

            }
        }
    }

    public void UnosOdgovora (ArrayList<String> odgovori, Pitanje pitanje, String dokumentPitanja) {

        SQLiteDatabase db = this.getWritableDatabase();

        for (int i=0; i<odgovori.size(); i++) {

            ContentValues vrijednosti = new ContentValues();
            vrijednosti.put(TEKST_ODGOVORA, odgovori.get(i));

            if (pitanje.getTacan().equals(odgovori.get(i)))
            vrijednosti.put(TACAN, 1);
            else
            vrijednosti.put(TACAN, 0);

            vrijednosti.put(ID_PITANJA, dokumentPitanja);


            db.insert(ODGOVORI_TABLE, null, vrijednosti);
        }

    }

    public void UnosRanglisti (String nazivKviza, ArrayList<Kviz> kvizovi, ArrayList<String> dokumentiKvizova, String dokumentRangliste) {

        SQLiteDatabase db = this.getWritableDatabase();

        System.out.println("Unos Rangliste:");
        System.out.println("Dokument rangliste: " + dokumentRangliste + ", Naziv kviza: " + nazivKviza);

            ContentValues vrijednosti = new ContentValues();
            vrijednosti.put(RANGLISTA_DOKUMENT, dokumentRangliste);
            vrijednosti.put(ID_KVIZA, nazivKviza);
            db.insert(RANGLISTE_TABLE, null, vrijednosti);

    }

    public void UnosKonkretneListe (String imenaIgraca, Double procentiTacnih, String dokumentRangliste) {

        SQLiteDatabase db = this.getWritableDatabase();

        System.out.println("Unos konkretne Rangliste: ");
        System.out.println("Dokument rangliste: " + dokumentRangliste + ", Ime igraca: " + imenaIgraca);

            ContentValues vrijednosti = new ContentValues();
            vrijednosti.put(IME_IGRACA, imenaIgraca);
            vrijednosti.put(PROCENAT_TACNIH, procentiTacnih);
            vrijednosti.put(ID_RANGLISTE, dokumentRangliste);
            db.insert(KONKRETNA_LISTA_TABLE, null, vrijednosti);

    }


    public ArrayList<Kategorija> UcitajKategorije() {
        ArrayList<Kategorija> kategorije = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] kolone = new String[]{KATEGORIJE_NAZIV, ID_IKONICE};
        Cursor cursor = db.query(KATEGORIJE_TABLE, kolone, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            do {
                Kategorija kategorija = new Kategorija(cursor.getString(0), cursor.getString(1));
                kategorije.add(kategorija);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return kategorije;
    }

    public ArrayList<String> UcitajDokumenteKategorija() {
        ArrayList<String> dokumenti = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] kolone = new String[]{KATEGORIJA_DOKUMENT, ID_IKONICE};
        Cursor cursor = db.query(KATEGORIJE_TABLE, kolone, null, null, null, null, null);

        if (cursor.moveToFirst()){

            do {
                dokumenti.add(cursor.getString(0));
            } while (cursor.moveToNext());

        }

        cursor.close();

        return dokumenti;
    }


    public ArrayList<Kviz> UcitajKvizove(ArrayList<Kategorija> kategorije, ArrayList<String> dokumentiKategorija, Kategorija potrebna, String dokumentPotrebne) {
            ArrayList<Kviz> kvizovi = new ArrayList<>();
            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{ID_KATEGORIJE, KVIZOVI_NAZIV, KVIZ_DOKUMENT};
            Cursor cursor = db.query(KVIZOVI_TABLE, kolone, null, null, null, null, null);

            if (cursor.moveToFirst()) {

                do {
                    Kviz kviz = null;
                    if (potrebna.getNaziv().equals("Svi")) {
                        for (int i = 0; i < dokumentiKategorija.size(); i++) {
                            if (dokumentiKategorija.get(i).equals(cursor.getString(0))) {
                                kviz = new Kviz(cursor.getString(1), new ArrayList<Pitanje>(), kategorije.get(i));
                                kviz = UcitajPitanjaZaKviz(kviz, cursor.getString(2));
                                kvizovi.add(kviz);
                            }
                        }
                        if (kviz == null) {
                            kviz = new Kviz(cursor.getString(1), new ArrayList<Pitanje>(), new Kategorija("Svi", "svi"));
                            kviz = UcitajPitanjaZaKviz(kviz, cursor.getString(2));
                            kvizovi.add(kviz);
                        }
                    } else {
                        if (dokumentPotrebne.equals(cursor.getString(0))) {
                            kviz = new Kviz(cursor.getString(1), new ArrayList<Pitanje>(), potrebna);
                            kviz = UcitajPitanjaZaKviz(kviz, cursor.getString(2));
                            kvizovi.add(kviz);
                        }
                    }
                } while (cursor.moveToNext());

            }

        cursor.close();

            return kvizovi;
        }


    public ArrayList<String> UcitajDokumenteKvizova (Kategorija kategorija, String dokumentKategorije) {

        ArrayList<String> dokumentiKvizova = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] kolone = new String[]{ID_KATEGORIJE, KVIZ_DOKUMENT};
        Cursor cursor = db.query(KVIZOVI_TABLE, kolone, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            do {

                if (kategorija.getNaziv().equals("Svi")) {
                    dokumentiKvizova.add(cursor.getString(1));
                } else {
                    if (dokumentKategorije.equals(cursor.getString(0)))
                        dokumentiKvizova.add(cursor.getString(1));
                }

            } while (cursor.moveToNext());

        }

        cursor.close();

        return dokumentiKvizova;

        }


        public Pitanje UcitajOdgovoreZaPitanje (Pitanje pitanje,String dokumentPitanja) {
            ArrayList<String> odgovori = new ArrayList<>();
            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{TEKST_ODGOVORA, ID_PITANJA, TACAN};
            Cursor cursor = db.query(ODGOVORI_TABLE, kolone, null, null, null, null, null);

            if (cursor.moveToFirst()) {

                do {

                    if (dokumentPitanja.equals(cursor.getString(1))) {
                        odgovori.add(cursor.getString(0));
                        if (Integer.parseInt(cursor.getString(2)) == 1)
                            pitanje.setTacan(cursor.getString(0));
                    }

                } while (cursor.moveToNext());

            }

            cursor.close();

            pitanje.setOdgovori(odgovori);

            return pitanje;
        }



        public Kviz UcitajPitanjaZaKviz (Kviz kviz, String dokumentKviza) {

        ArrayList<Pitanje> pitanja = new ArrayList<>();

            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{PITANJE_DOKUMENT, PITANJA_NAZIV, ID_KVIZA};
            Cursor cursor = db.query(PITANJA_TABLE, kolone, null, null, null, null, null);

            if (cursor.moveToFirst()) {

                do {

                    if (dokumentKviza.equals(cursor.getString(2))) {
                        Pitanje pitanje = new Pitanje(cursor.getString(1), cursor.getString(1), new ArrayList<String>(), "");
                        pitanje = UcitajOdgovoreZaPitanje(pitanje, cursor.getString(0));
                        pitanja.add(pitanje);
                    }

                } while (cursor.moveToNext());

            }

            cursor.close();

            kviz.setPitanja(pitanja);

            return kviz;

        }


        public void UcitajKonkretnuRanglistu (String dokumentRangliste, ArrayList<String> igraci, ArrayList<Double> procenti) {

            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{ID_RANGLISTE, IME_IGRACA, PROCENAT_TACNIH};
            Cursor cursor = db.query(KONKRETNA_LISTA_TABLE, kolone, null, null, null, null, null);

            if (cursor.moveToFirst()) {

                do {

                    if (dokumentRangliste.equals(cursor.getString(0))) {
                        igraci.add(cursor.getString(1));
                        procenti.add(cursor.getDouble(2));
                    }

                } while (cursor.moveToNext());

            }

            cursor.close();

        }


        public void UcitajRanglistu (String nazivKviza, ArrayList<String> igraci, ArrayList<Double> procenti) {

            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{RANGLISTA_DOKUMENT, ID_KVIZA};
            Cursor cursor = db.query(RANGLISTE_TABLE, kolone, null, null, null, null, null);

            if (cursor.moveToFirst()) {

                do {

                    if (nazivKviza.equals(cursor.getString(1))) {
                        UcitajKonkretnuRanglistu(cursor.getString(0), igraci, procenti);
                        break;
                    }


                } while (cursor.moveToNext());

            }

            cursor.close();

        }


        public String DajDokumentRangliste (String nazivKviza)
        {

            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{RANGLISTA_DOKUMENT, ID_KVIZA};
            Cursor cursor = db.query(RANGLISTE_TABLE, kolone, null, null, null, null, null);
            String dokument = "";

            if (cursor.moveToFirst()) {

                do {

                    if (nazivKviza.equals(cursor.getString(1))) {
                        dokument = cursor.getString(0);
                        break;
                    }


                } while (cursor.moveToNext());

            }

            cursor.close();

            System.out.println("Davanje dokumenta Rangliste: " + dokument);

            return dokument;

        }


    public String DajKvizRangliste (String dokument)
    {

        SQLiteDatabase db = getReadableDatabase();
        String[] kolone = new String[]{RANGLISTA_DOKUMENT, ID_KVIZA};
        Cursor cursor = db.query(RANGLISTE_TABLE, kolone, null, null, null, null, null);
        String naziv = "";

        if (cursor.moveToFirst()) {

            do {

                if (dokument.equals(cursor.getString(0))) {
                    naziv = cursor.getString(1);
                    break;
                }


            } while (cursor.moveToNext());

        }

        cursor.close();

        System.out.println("Davanje naziva kviza Rangliste: " + naziv);

        return naziv;

    }


        public String AzirirajRanglistu (ArrayList<String> imenaIgraca, ArrayList<Double> procentiIgraca, ArrayList<String> dokumentiRanglisti) {

        String dokumentRangliste = "";

            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{RANGLISTA_DOKUMENT, ID_KVIZA};
            Cursor cursor = db.query(RANGLISTE_TABLE, kolone, null, null, null, null, null);

            if (cursor.moveToFirst()) {

                do {

                    if (dokumentiRanglisti.size() != 0) {
                        if (!dokumentiRanglisti.contains(cursor.getString(0))) {
                            UcitajKonkretnuRanglistu(cursor.getString(0), imenaIgraca, procentiIgraca);
                            SortirajIgrace(imenaIgraca, procentiIgraca);
                            dokumentRangliste = cursor.getString(0);
                            break;
                        }
                    } else {
                        UcitajKonkretnuRanglistu(cursor.getString(0), imenaIgraca, procentiIgraca);
                        SortirajIgrace(imenaIgraca, procentiIgraca);
                        dokumentRangliste = cursor.getString(0);
                        break;
                    }

                } while (cursor.moveToNext());

            }

            cursor.close();

        return dokumentRangliste;
        }


        public int DajBrojRanglisti () {

            SQLiteDatabase db = getReadableDatabase();
            String[] kolone = new String[]{RANGLISTA_DOKUMENT, ID_KVIZA};
            Cursor cursor = db.query(RANGLISTE_TABLE, kolone, null, null, null, null, null);
            int rez = 0;

            if (cursor.moveToFirst()){

                do {
                    rez++;
                } while (cursor.moveToNext());

            }

            cursor.close();

            return rez;

        }


        public void SortirajIgrace (ArrayList<String> imenaIgraca, ArrayList<Double> procentiIgraca) {

            if (imenaIgraca.size() > 1) {

                String[] pomImena = new String[imenaIgraca.size()];
                int[] pomProcenti = new int[procentiIgraca.size()];

                for (int i = 0; i < procentiIgraca.size(); i++) {
                    pomImena[i] = imenaIgraca.get(i);
                    pomProcenti[i] = procentiIgraca.get(i).intValue();
                }

                for (int i = 0; i <= pomImena.length - 2; i++) {
                    int max = pomProcenti[i];
                    int pmax = i;
                    String max1 = pomImena[i];
                    for (int j = i + 1; j <= pomImena.length - 1; j++) {
                        if ( pomProcenti[j] > max) {
                            max = pomProcenti[j];
                            max1 = pomImena[j];
                            pmax = j;
                        }
                    }
                    pomProcenti[pmax] = pomProcenti[i];
                    pomImena[pmax] = pomImena[i];
                    pomProcenti[i] = max;
                    pomImena[i] = max1;
                }

                imenaIgraca.clear();
                procentiIgraca.clear();

                for (int i = 0; i < pomImena.length; i++) {
                    imenaIgraca.add(pomImena[i]);
                    procentiIgraca.add((double) pomProcenti[i]);
                }
            }

        }

}

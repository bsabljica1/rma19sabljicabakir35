package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Pitanje;


public class PitanjeFrag extends Fragment {

    private Pitanje pitanje;
    private ArrayList<String> odgovori = new ArrayList<>();
    private OnItemClick oic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pitanje_frag, container, false);
    }



    @Override
    public void onActivityCreated (Bundle savedInstanceBundle) {
        super.onActivityCreated(savedInstanceBundle);


        if (getArguments().containsKey("odabranoPitanje")) {
            pitanje = (Pitanje) getArguments().getSerializable("odabranoPitanje");
            TextView textView = (TextView) getView().findViewById(R.id.tekstPitanja);
            final ListView listView = (ListView) getView().findViewById(R.id.odgovoriPitanja);
            textView.setText(pitanje.getNaziv());
            if (pitanje.getOdgovori()!=null)
            odgovori = pitanje.dajRandomOdgovore();
            else odgovori = new ArrayList<>();
            ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, odgovori);
            listView.setAdapter(adapter);


            try {
                oic = (OnItemClick) getActivity();
            } catch (ClassCastException e) {e.getMessage();}
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (odgovori.get(position).equals(pitanje.getTacan())) {
                        view.setBackgroundColor(getResources().getColor(R.color.zelena));
                    }
                    else {
                        view.setBackgroundColor(getResources().getColor(R.color.crvena));
                        int indeksTacnog = 0;
                        for (String odg : odgovori) {
                            if (odg.equals(pitanje.getTacan())) break;
                            indeksTacnog++;
                        }
                        listView.getChildAt(indeksTacnog).setBackgroundColor(getResources().getColor(R.color.zelena));
                    }
                    oic.onItemClicked(position);
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public interface OnItemClick {
        public void onItemClicked(int pos);
    }


}

package ba.unsa.etf.rma;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.Kviz;

public class AdapterKvizGridView extends BaseAdapter {

    Context context;
    ArrayList<Kviz> kvizovi = new ArrayList<>();
   private LayoutInflater inflater;

    public AdapterKvizGridView(Context applicationContext, ArrayList<Kviz> kvizs) {
        this.context = applicationContext;
        this.kvizovi = kvizs;
        inflater = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return kvizovi.size();
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        int brojPitanja = 0;
        if (kvizovi.get(position).getPitanja()!=null) brojPitanja=kvizovi.get(position).getPitanja().size();
        v = inflater.inflate(R.layout.ikone_grid_view, null);
        IconView iconView = (IconView) v.findViewById(R.id.slike);
        TextView textView = (TextView) v.findViewById(R.id.textView);
        textView.setText("Naziv kviza:\n"+kvizovi.get(position).getNaziv()+"\n"+"Broj pitanja:\n"+brojPitanja);

        if (kvizovi.get(position).getNaziv().equals("Dodaj kviz")) {
            v = inflater.inflate(R.layout.element_liste_grid_view, null);
            ImageView imageView = v.findViewById(R.id.slike);
            textView = (TextView) v.findViewById(R.id.textView);
            textView.setText("Naziv kviza:\n"+kvizovi.get(position).getNaziv());
            imageView.setImageResource(R.drawable.dodajdugme);
        }

        else {
            if (kvizovi.get(position).getKategorija().getId().equals("svi")) {
                v = inflater.inflate(R.layout.element_liste_grid_view, null);
                ImageView imageView = v.findViewById(R.id.slike);
                textView = (TextView) v.findViewById(R.id.textView);
                textView.setText("Naziv kviza:\n"+kvizovi.get(position).getNaziv()+"\n"+"Broj pitanja:\n"+brojPitanja);
                imageView.setImageResource(R.drawable.crnodugme);
            }
            else
                iconView.setIcon(Integer.parseInt(kvizovi.get(position).getKategorija().getId()));
        }

        return v;

    }
}

package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.AdapterKategorijaLista;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;

public class ListaFrag extends Fragment {

    private ArrayList<Kategorija> listaKategorija = new ArrayList<>();
    private ListView listView;
    private AdapterKategorijaLista adapterKategorijaLista;
    private OnItemClick oic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lista_frag, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceBundle) {
        super.onActivityCreated(savedInstanceBundle);

        if (getArguments().containsKey("listaKategorija")) {
            listView = (ListView) getView().findViewById(R.id.listaKategorija);
            listaKategorija = (ArrayList<Kategorija>) getArguments().getSerializable("listaKategorija");
            adapterKategorijaLista = new AdapterKategorijaLista(getContext(), R.layout.lista_kategorije, listaKategorija);
            listView.setAdapter(adapterKategorijaLista);


            try {
                oic = (ListaFrag.OnItemClick) getActivity();
            } catch (ClassCastException e) {e.getMessage();}
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    oic.onItemClicked(position);
                }
            });
        }

    }


    public interface OnItemClick {
        public void onItemClicked(int pos);
    }

}

package ba.unsa.etf.rma.fragmenti;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ba.unsa.etf.rma.AdapterKviz;
import ba.unsa.etf.rma.AdapterKvizGridView;
import ba.unsa.etf.rma.BazaDBOpenHelper;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DetailFrag extends Fragment {

    private GridView gridView;
    private ArrayList<Kategorija> kategorije = new ArrayList<>();
    private ArrayList<Kviz> potrebniKvizovi = new ArrayList<>();
    private AdapterKvizGridView adapterKviz;
    Kviz dodajKviz = new Kviz("Dodaj kviz", null, null);
    private ArrayList<String> dokumentKviza = new ArrayList<>();
    private ArrayList<String> ucitaniIgraci = new ArrayList<>();
    private ArrayList<Double> ucitaniProcenti = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.detail_frag, container, false);

        if (getArguments()!=null) {


            gridView = (GridView) iv.findViewById(R.id.gridKvizovi);
            potrebniKvizovi = (ArrayList<Kviz>) getArguments().getSerializable("potrebniKvizovi");
            adapterKviz = new AdapterKvizGridView(getContext(), potrebniKvizovi);
            gridView.setAdapter(adapterKviz);
            kategorije = (ArrayList<Kategorija>) getArguments().getSerializable("kategorije");
            dokumentKviza = (ArrayList<String>) getArguments().getSerializable("dokumentKviza");
            ucitaniIgraci = (ArrayList<String>) getArguments().getSerializable("ucitaniIgraci");
            ucitaniProcenti = (ArrayList<Double>) getArguments().getSerializable("ucitaniProcenti");



            gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    if (!getArguments().getBoolean("konekcija")) {
                        return false;
                    }

                    Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);

                    String azuriranje = "ne";

                    if (!potrebniKvizovi.get(position).getNaziv().equals("Dodaj kviz")) {

                            azuriranje = "da";
                            myIntent.putExtra("dokumentKviza", dokumentKviza.get(position));


                    } else {

                        myIntent.putExtra("azuriranje", azuriranje);

                    }


                    getActivity().startActivity(myIntent);

                    return true;

                }
            });


            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if (potrebniKvizovi.get(position).getNaziv().equals("Dodaj kviz")) return;

                    Intent igranjeKviza = new Intent(getActivity(), IgrajKvizAkt.class);
                    igranjeKviza.putExtra("dokumentKviza", dokumentKviza.get(position));
                    igranjeKviza.putExtra("kviz", potrebniKvizovi.get(position));

                    BazaDBOpenHelper bazaDBOpenHelper = new BazaDBOpenHelper(getContext(), BazaDBOpenHelper.DATABASE_NAME, null, BazaDBOpenHelper.DATABASE_VERSION);
                    bazaDBOpenHelper.UcitajRanglistu(potrebniKvizovi.get(position).getNaziv(), ucitaniIgraci, ucitaniProcenti);
                    igranjeKviza.putExtra("ucitaniIgraci", ucitaniIgraci);
                    igranjeKviza.putExtra("ucitaniProcenti", ucitaniProcenti);
                    getActivity().startActivity(igranjeKviza);


                }
            });

        }



        return iv;
    }

}

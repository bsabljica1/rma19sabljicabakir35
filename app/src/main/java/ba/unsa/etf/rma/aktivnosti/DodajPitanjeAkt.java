package ba.unsa.etf.rma.aktivnosti;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import ba.unsa.etf.rma.AdapterOdgovori;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajPitanjeAkt extends AppCompatActivity {

   private String tacanOdgovor = new String();
   private ArrayList<Pitanje> listaPitanja = new ArrayList<>();
   private Kviz trenutniKviz;
   private String naziv = new String();
    private EditText nazivPitanja;
    private EditText odgovorNaPitanje;
    private ListView listaOdgovora;
    private  Button dodajOdgovor;
    private  Button dodajTacanOdgovor;
    private   Button dodajPitanje;
    private ArrayList<String> odgovori = new ArrayList<String>();
    private Pitanje novoPitanje;
    private ArrayList<String> dokumentiPitanja = new ArrayList<>();
    private AdapterOdgovori adapter;
    private Context context = this;





    public class ProcitajBazuPitanja extends AsyncTask<String, Void, Void> {

        private Context con;

        public ProcitajBazuPitanja(Context c) {
            con = c;
        }

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumenPitanja = dijelovi[6];
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivPitanja = konkretnoPitanje.getJSONObject("naziv");
                            JSONObject indexTacnogPitanja = konkretnoPitanje.getJSONObject("indexTacnog");
                            JSONObject lOdg = konkretnoPitanje.getJSONObject("odgovori");
                            JSONObject lOdgovora = lOdg.getJSONObject("arrayValue");
                            JSONArray listaOdgovora = lOdgovora.getJSONArray("values");

                            ArrayList<String> odgovori = new ArrayList<>();
                            for (int j=0; j<listaOdgovora.length(); j++) {
                                JSONObject jedanOdg = listaOdgovora.getJSONObject(j);
                                String value = jedanOdg.getString("stringValue");
                                odgovori.add(value);
                            }

                            String n = nazivPitanja.getString("stringValue");
                            String index = indexTacnogPitanja.getString("integerValue");
                            boolean postojiPitanje = false;
                            for (int j=0; j<listaPitanja.size(); j++) {
                                if (listaPitanja.get(j).getNaziv().equals(n))
                                    postojiPitanje = true;
                            }


                            if (!postojiPitanje) {
                                listaPitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));
                                dokumentiPitanja.add(dokumenPitanja);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            naziv = nazivPitanja.getText().toString();
            if (!validacijaNazivaPitanja()) return;
                if (odgovori.size() == 0) return;
                if (tacanOdgovor == null || tacanOdgovor.length() == 0) return;

                novoPitanje = new Pitanje(naziv, naziv, odgovori, tacanOdgovor);
                trenutniKviz = (Kviz) getIntent().getSerializableExtra("kviz");
                trenutniKviz.getPitanja().add(novoPitanje);
                new KreirajPitanjeTask().execute("projekat");

        }
    }













    public class KreirajPitanjeTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int indexTacnog = -1;
                for (int i=0; i<novoPitanje.getOdgovori().size(); i++) {
                    if (novoPitanje.getOdgovori().get(i).equals(novoPitanje.getTacan())) indexTacnog = i;
                }

                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+ novoPitanje.getNaziv() +"\"}, \"indexTacnog\": {\"integerValue\":\""+ indexTacnog +"\"}" +
                        ", \"odgovori\": {\"arrayValue\": {\"values\": [" ;
                String odgovori="";
                for (int i=0; i<novoPitanje.getOdgovori().size(); i++) {
                    odgovori += "{\"stringValue\": \""+novoPitanje.getOdgovori().get(i)+"\"}";
                    if (i!=novoPitanje.getOdgovori().size()-1) odgovori+=", ";
                }
                dokument+=odgovori+"]}} }}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent aktivnostDodavanjaKviza = new Intent(DodajPitanjeAkt.this, DodajKvizAkt.class);
            aktivnostDodavanjaKviza.putExtra("dokumentKviza", getIntent().getStringExtra("dokumentKviza"));
            aktivnostDodavanjaKviza.putExtra("azuriranje", getIntent().getStringExtra("azuriranje"));
            aktivnostDodavanjaKviza.putExtra("kviz", trenutniKviz);
            DodajPitanjeAkt.this.startActivity(aktivnostDodavanjaKviza);
        }
    }

















    private boolean validacijaNazivaPitanja () {

        if (naziv.length()==0) {
            nazivPitanja.setBackgroundColor(Color.parseColor("#ffaaaa"));
            return false;
        }
        if (listaPitanja!=null)
        for (int i=0; i<listaPitanja.size(); i++)
            if (listaPitanja.get(i).getNaziv().equals(naziv)) {
                nazivPitanja.setBackgroundColor(Color.parseColor("#ffaaaa"));
                return false;
            }

        return true;

    }




    private void bojaPozadine(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                return;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void afterTextChanged(Editable s) {
                return;
            }
        });
    }



    public boolean ProvjeraKonekcije() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else
            connected = false;

        return connected;
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_pitanje_akt);
        nazivPitanja = (EditText) findViewById(R.id.etNaziv);
        odgovorNaPitanje = (EditText) findViewById(R.id.etOdgovor);
        listaOdgovora = (ListView) findViewById(R.id.lvOdgovori);
        dodajOdgovor = (Button) findViewById(R.id.btnDodajOdgovor);
        dodajTacanOdgovor = (Button) findViewById(R.id.btnDodajTacan);
        dodajPitanje = (Button) findViewById(R.id.btnDodajPitanje);
        context = this;

        if (!ProvjeraKonekcije()) {
            Toast.makeText(DodajPitanjeAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(DodajPitanjeAkt.this, KvizoviAkt.class);
            DodajPitanjeAkt.this.startActivity(myIntent);
        }



        adapter = new AdapterOdgovori(this, R.layout.element_liste, odgovori) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position,convertView,parent);
                if(odgovori.get(position).equals(tacanOdgovor))
                {
                    view.setBackgroundColor(Color.GREEN);
                }
                else
                {
                    view.setBackgroundColor(Color.WHITE);
                }
                return view;
            }
        };

        listaOdgovora.setAdapter(adapter);


        dodajOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajPitanjeAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajPitanjeAkt.this, KvizoviAkt.class);
                    DodajPitanjeAkt.this.startActivity(myIntent);
                }

                if (odgovorNaPitanje.getText()!=null && odgovorNaPitanje.getText().toString().length()!=0)  {
                    for (int i=0; i<odgovori.size(); i++) {
                        if (odgovorNaPitanje.getText().toString().equals(odgovori.get(i))) return;
                    }
                    odgovori.add(odgovorNaPitanje.getText().toString());
                    adapter.notifyDataSetChanged();
                }
            }
        });


        listaOdgovora.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajPitanjeAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajPitanjeAkt.this, KvizoviAkt.class);
                    DodajPitanjeAkt.this.startActivity(myIntent);
                }

                if (odgovori.get(position).equals(tacanOdgovor)) {
                    tacanOdgovor = null;
                }
                odgovori.remove(position);
                adapter.notifyDataSetChanged();

            }
        });


        dodajTacanOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajPitanjeAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajPitanjeAkt.this, KvizoviAkt.class);
                    DodajPitanjeAkt.this.startActivity(myIntent);
                }

                if (odgovorNaPitanje.getText()!=null && odgovorNaPitanje.getText().toString().length()!=0) {
                    for (int i=0; i<odgovori.size(); i++) {
                        if (odgovorNaPitanje.getText().toString().equals(odgovori.get(i))) return;
                    }

                    if (odgovori.contains(tacanOdgovor)) return;
                    tacanOdgovor=odgovorNaPitanje.getText().toString();
                    odgovori.add(odgovorNaPitanje.getText().toString());
                    adapter.notifyDataSetChanged();
                }
            }
        });

        dodajPitanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ProvjeraKonekcije()) {
                    Toast.makeText(DodajPitanjeAkt.this, "Nema konekcije na internet. Povratak na pocetnu aktivnost", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(DodajPitanjeAkt.this, KvizoviAkt.class);
                    DodajPitanjeAkt.this.startActivity(myIntent);
                }


                new ProcitajBazuPitanja(context).execute("dsd");
                Toast.makeText(DodajPitanjeAkt.this, "Validacija i dodavanje pitanja u toku", Toast.LENGTH_LONG).show();

            }
        });


        bojaPozadine(nazivPitanja);

    }

}
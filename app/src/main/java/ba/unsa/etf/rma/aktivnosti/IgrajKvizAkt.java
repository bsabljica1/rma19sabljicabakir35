package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.AlarmClock;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

import ba.unsa.etf.rma.BazaDBOpenHelper;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class IgrajKvizAkt extends AppCompatActivity  implements PitanjeFrag.OnItemClick {

    private Kviz kviz;
    private Pitanje odabranoPitanje;
    private InformacijeFrag informacijeFrag;
    private PitanjeFrag pitanjeFrag;
    private FragmentManager fragmentManager;
    private FrameLayout pitanjeFrameLayout;
    private FrameLayout informacijeFrameLayout;
    private int ukupni =0;
    private int brojPreostalihPitanja = 0;
    private int izabranoPitanje = 0;
    private int brojTacnih =0;
    private String dokumentPotrebnogKviza;
    private String nazivKviza;
    private String potrebnaKategorija;
    private ArrayList<String> potrebnaPitanja = new ArrayList<>();
    private Kategorija pronadjenaKategorija;
    private ArrayList<Pitanje> listaTrenutnihPitanja = new ArrayList<>();
    private String imeIgraca;
    private Context con=this;
    private ArrayList<String> ucitaniIgraci = new ArrayList<>();
    private ArrayList<Double> ucitaniProcenti = new ArrayList<>();


    public class PronadjiPotrebniKviz extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumentKviza = dijelovi[6];
                            JSONObject konkretanKviz = jedno.getJSONObject("fields");
                            JSONObject nazivKvizaa = konkretanKviz.getJSONObject("naziv");
                            JSONObject idKat = konkretanKviz.getJSONObject("idKategorije");
                            JSONObject lPit = konkretanKviz.getJSONObject("pitanja");
                            JSONObject lPitanja = lPit.getJSONObject("arrayValue");
                            ArrayList<String> pitanja = new ArrayList<>();
                            if (lPitanja.has("values")) {
                                JSONArray listaPitanja = lPitanja.getJSONArray("values");


                                for (int j = 0; j < listaPitanja.length(); j++) {
                                    JSONObject jedanOdg = listaPitanja.getJSONObject(j);
                                    String value = jedanOdg.getString("stringValue");
                                    pitanja.add(value); // LISTA ID PITANJA
                                }

                            }

                            String n = nazivKvizaa.getString("stringValue"); //NAZIV KVIZA
                            String idKatString = idKat.getString("stringValue"); // ID KATEGORIJE

                            if (dokumentPotrebnogKviza.equals(dokumentKviza)) {
                                nazivKviza = n;
                                potrebnaKategorija = idKatString;
                                potrebnaPitanja = pitanja;
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new PronadjiPotrebnuKategoriju().execute("dsd");
        }
    }



    public class PronadjiPotrebnuKategoriju extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String nazivDokumenta = kata.getString("name");
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            JSONObject idKate = konkretnaKata.getJSONObject("idIkonice");
                            String n = nazivKate.getString("stringValue");
                            String id = idKate.getString("integerValue");

                            String[] dijelovi = nazivDokumenta.split("/");

                            if (potrebnaKategorija.equals(dijelovi[6])) {
                                pronadjenaKategorija = new Kategorija(n, id);
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new PronadjiPotrebnaPitanja().execute("dsd");
        }
    }



    public class PronadjiPotrebnaPitanja extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumenPitanja = dijelovi[6];
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivPitanja = konkretnoPitanje.getJSONObject("naziv");
                            JSONObject indexTacnogPitanja = konkretnoPitanje.getJSONObject("indexTacnog");
                            JSONObject lOdg = konkretnoPitanje.getJSONObject("odgovori");
                            JSONObject lOdgovora = lOdg.getJSONObject("arrayValue");
                            JSONArray listaOdgovora = lOdgovora.getJSONArray("values");

                            ArrayList<String> odgovori = new ArrayList<>();
                            for (int j=0; j<listaOdgovora.length(); j++) {
                                JSONObject jedanOdg = listaOdgovora.getJSONObject(j);
                                String value = jedanOdg.getString("stringValue");
                                odgovori.add(value); //LISTA ODGOVORA
                            }

                            String n = nazivPitanja.getString("stringValue"); //NAZIV PITANJA
                            String index = indexTacnogPitanja.getString("integerValue"); //INDEX TACNOG

                                boolean pronadjenoPitanje = false;
                                for (int j = 0; j < potrebnaPitanja.size(); j++) {
                                    if (potrebnaPitanja.get(j).equals(dokumenPitanja)) {
                                        listaTrenutnihPitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));
                                        pronadjenoPitanje = true;
                                        break;
                                    }
                                }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            kviz = new Kviz(nazivKviza, listaTrenutnihPitanja, pronadjenaKategorija);

            if (kviz.getPitanja().size()>0) {


                Intent openNewAlarm = new Intent(AlarmClock.ACTION_SET_ALARM);
                openNewAlarm.putExtra(AlarmClock.EXTRA_MESSAGE, "Vrijeme za igranje kviza je isteklo.");
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);
                int brojPitanja = kviz.getPitanja().size() / 2;
                if (kviz.getPitanja().size() % 2 != 0)
                    brojPitanja++;
                openNewAlarm.putExtra(AlarmClock.EXTRA_HOUR, hour);
                openNewAlarm.putExtra(AlarmClock.EXTRA_MINUTES,  minute + 1 + brojPitanja);
                System.out.println(hour + ":" + (minute+1+brojPitanja));
                openNewAlarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                startActivity(openNewAlarm);

            }

            if (kviz.getPitanja()!=null && kviz.getPitanja().size()>0)
                brojPreostalihPitanja = kviz.getPitanja().size();

            if (kviz.getPitanja()!=null && kviz.getPitanja().size()>0) {

                fragmentManager = getSupportFragmentManager();
                pitanjeFrameLayout = (FrameLayout) findViewById(R.id.pitanjePlace);
                pitanjeFrag = (PitanjeFrag) fragmentManager.findFragmentById(R.id.pitanjePlace);

                if (pitanjeFrag == null) {
                    pitanjeFrag = new PitanjeFrag();
                    Bundle argumenti = new Bundle();
                    long seed = System.nanoTime();
                    Collections.shuffle(kviz.getPitanja(), new Random(seed));
                    odabranoPitanje = kviz.getPitanja().get(izabranoPitanje);
                    brojPreostalihPitanja--;
                    argumenti.putSerializable("odabranoPitanje", odabranoPitanje);
                    pitanjeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
                }

                fragmentManager = getSupportFragmentManager();
                informacijeFrameLayout = (FrameLayout) findViewById(R.id.informacijePlace);
                informacijeFrag = (InformacijeFrag) fragmentManager.findFragmentById(R.id.informacijePlace);

                if (informacijeFrag == null) {
                    informacijeFrag = new InformacijeFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("kviz", kviz);
                    argumenti.putInt("brojPreostalih", brojPreostalihPitanja);
                    argumenti.putInt("brojTacnih", brojTacnih);
                    argumenti.putInt("ukupniBrojPitanja", ukupni);
                    informacijeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();
                }

            }

            else {

                fragmentManager = getSupportFragmentManager();
                pitanjeFrameLayout = (FrameLayout) findViewById(R.id.pitanjePlace);
                pitanjeFrag = (PitanjeFrag) fragmentManager.findFragmentById(R.id.pitanjePlace);

                if (pitanjeFrag == null) {
                    pitanjeFrag = new PitanjeFrag();
                    Bundle argumenti = new Bundle();
                    odabranoPitanje = new Pitanje("Kviz je zavrsen", null, null, null);
                    argumenti.putSerializable("odabranoPitanje", odabranoPitanje);
                    pitanjeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
                }

                fragmentManager = getSupportFragmentManager();
                informacijeFrameLayout = (FrameLayout) findViewById(R.id.informacijePlace);
                informacijeFrag = (InformacijeFrag) fragmentManager.findFragmentById(R.id.informacijePlace);

                if (informacijeFrag == null) {
                    informacijeFrag = new InformacijeFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("kviz", kviz);
                    argumenti.putInt("brojPreostalih", 0);
                    argumenti.putInt("brojTacnih", 0);
                    argumenti.putInt("ukupniBrojPitanja", 0);
                    informacijeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();
                }
            }
        }
    }


    public boolean ProvjeraKonekcije() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else
            connected = false;

        return connected;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.igraj_kviz_akt);

        if (ProvjeraKonekcije()) {

            dokumentPotrebnogKviza = getIntent().getStringExtra("dokumentKviza");
            new PronadjiPotrebniKviz().execute("dsd");

        }


        else {



            kviz = (Kviz) getIntent().getSerializableExtra("kviz");
            ucitaniIgraci = (ArrayList<String>) getIntent().getSerializableExtra("ucitaniIgraci");
            ucitaniProcenti = (ArrayList<Double>) getIntent().getSerializableExtra("ucitaniProcenti");
            nazivKviza = kviz.getNaziv();
            listaTrenutnihPitanja = kviz.getPitanja();
            pronadjenaKategorija = kviz.getKategorija();

            if (kviz.getPitanja().size()>0) {

                Intent openNewAlarm = new Intent(AlarmClock.ACTION_SET_ALARM);
                openNewAlarm.putExtra(AlarmClock.EXTRA_MESSAGE, "Vrijeme za igranje kviza je isteklo.");
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);
                int brojPitanja = kviz.getPitanja().size() / 2;
                if (kviz.getPitanja().size() % 2 != 0)
                    brojPitanja++;
                openNewAlarm.putExtra(AlarmClock.EXTRA_HOUR, hour);
                openNewAlarm.putExtra(AlarmClock.EXTRA_MINUTES,  minute + 1 + brojPitanja);
                System.out.println(hour + ":" + (minute+1+brojPitanja));
                openNewAlarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                startActivity(openNewAlarm);

            }

            if (kviz.getPitanja()!=null && kviz.getPitanja().size()>0)
                brojPreostalihPitanja = kviz.getPitanja().size();

            if (kviz.getPitanja()!=null && kviz.getPitanja().size()>0) {

                fragmentManager = getSupportFragmentManager();
                pitanjeFrameLayout = (FrameLayout) findViewById(R.id.pitanjePlace);
                pitanjeFrag = (PitanjeFrag) fragmentManager.findFragmentById(R.id.pitanjePlace);

                if (pitanjeFrag == null) {
                    pitanjeFrag = new PitanjeFrag();
                    Bundle argumenti = new Bundle();
                    long seed = System.nanoTime();
                    Collections.shuffle(kviz.getPitanja(), new Random(seed));
                    odabranoPitanje = kviz.getPitanja().get(izabranoPitanje);
                    brojPreostalihPitanja--;
                    argumenti.putSerializable("odabranoPitanje", odabranoPitanje);
                    pitanjeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
                }

                fragmentManager = getSupportFragmentManager();
                informacijeFrameLayout = (FrameLayout) findViewById(R.id.informacijePlace);
                informacijeFrag = (InformacijeFrag) fragmentManager.findFragmentById(R.id.informacijePlace);

                if (informacijeFrag == null) {
                    informacijeFrag = new InformacijeFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("kviz", kviz);
                    argumenti.putInt("brojPreostalih", brojPreostalihPitanja);
                    argumenti.putInt("brojTacnih", brojTacnih);
                    argumenti.putInt("ukupniBrojPitanja", ukupni);
                    informacijeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();
                }

            }

            else {

                fragmentManager = getSupportFragmentManager();
                pitanjeFrameLayout = (FrameLayout) findViewById(R.id.pitanjePlace);
                pitanjeFrag = (PitanjeFrag) fragmentManager.findFragmentById(R.id.pitanjePlace);

                if (pitanjeFrag == null) {
                    pitanjeFrag = new PitanjeFrag();
                    Bundle argumenti = new Bundle();
                    odabranoPitanje = new Pitanje("Kviz je zavrsen", null, null, null);
                    argumenti.putSerializable("odabranoPitanje", odabranoPitanje);
                    pitanjeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
                }

                fragmentManager = getSupportFragmentManager();
                informacijeFrameLayout = (FrameLayout) findViewById(R.id.informacijePlace);
                informacijeFrag = (InformacijeFrag) fragmentManager.findFragmentById(R.id.informacijePlace);

                if (informacijeFrag == null) {
                    informacijeFrag = new InformacijeFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("kviz", kviz);
                    argumenti.putInt("brojPreostalih", 0);
                    argumenti.putInt("brojTacnih", 0);
                    argumenti.putInt("ukupniBrojPitanja", 0);
                    informacijeFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();
                }
            }

        }

    }



    public void onItemClicked (int pos) {

        brojPreostalihPitanja--;
        ukupni++;
        izabranoPitanje++;
        if (brojPreostalihPitanja>=0) {

            if (odabranoPitanje.getOdgovori().get(pos).equals(odabranoPitanje.getTacan())) brojTacnih++;


            Bundle argumenti = new Bundle();
            informacijeFrag = new InformacijeFrag();
            argumenti.putSerializable("kviz", kviz);
            argumenti.putInt("brojTacnih", brojTacnih);
            argumenti.putInt("brojPreostalih", brojPreostalihPitanja);
            argumenti.putInt("ukupniBrojPitanja", ukupni);
            informacijeFrag.setArguments(argumenti);
            getSupportFragmentManager().beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();

            Bundle argumenti2 = new Bundle();
            pitanjeFrag = new PitanjeFrag();
            if (izabranoPitanje<kviz.getPitanja().size())
            odabranoPitanje = kviz.getPitanja().get(izabranoPitanje);
            argumenti2.putSerializable("odabranoPitanje", odabranoPitanje);
            pitanjeFrag.setArguments(argumenti2);
            fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
        }
        else {

            Intent closeNewAlarm = new Intent(AlarmClock.ACTION_DISMISS_ALARM);
            closeNewAlarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
            startActivity(closeNewAlarm);

            if (odabranoPitanje.getOdgovori().get(pos).equals(odabranoPitanje.getTacan())) brojTacnih++;
            Bundle argumenti = new Bundle();
            informacijeFrag = new InformacijeFrag();
            argumenti.putSerializable("kviz", kviz);
            argumenti.putInt("brojTacnih", brojTacnih);
            argumenti.putInt("brojPreostalih", 0);
            argumenti.putInt("ukupniBrojPitanja", kviz.getPitanja().size());
            informacijeFrag.setArguments(argumenti);
            getSupportFragmentManager().beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();

            Bundle argumenti2 = new Bundle();
            pitanjeFrag = new PitanjeFrag();
            odabranoPitanje = new Pitanje("Kviz je zavrsen", null, null, null);
            argumenti2.putSerializable("odabranoPitanje", odabranoPitanje);
            pitanjeFrag.setArguments(argumenti2);
            fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();


            final AlertDialog alertDialog = new AlertDialog.Builder(IgrajKvizAkt.this).create();
            alertDialog.setTitle("Kviz je zavrsen");
            alertDialog.setMessage("Unesite vase ime i prezime, a zatim pritisnite na dugme \"OK\" ukoliko zelite da se upisete u rang listu.");
            final EditText input = new EditText(this);
            imeIgraca = "";
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
            alertDialog.setView(input);
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "PONISTI",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }
                    );
            alertDialog.show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (input.getText().toString().length()!=0) {
                        imeIgraca = input.getText().toString();
                        Bundle argumenti2 = new Bundle();
                        RangLista rangLista = new RangLista();
                        argumenti2.putSerializable("konekcija", ProvjeraKonekcije());
                        argumenti2.putSerializable("imeIgraca", imeIgraca);
                        argumenti2.putSerializable("nazivKviza", kviz.getNaziv());
                        argumenti2.putSerializable("ucitaniIgraci", ucitaniIgraci);
                        argumenti2.putSerializable("ucitaniProcenti", ucitaniProcenti);
                        double procent = 0;
                        if (ukupni!=0) procent =((double)brojTacnih/(double)ukupni)*100;
                        else procent = 0;
                        argumenti2.putSerializable("procenatTacnih", procent);
                        rangLista.setArguments(argumenti2);
                        fragmentManager.beginTransaction().replace(R.id.pitanjePlace, rangLista).commit();
                        alertDialog.dismiss();
                    }
                    else {
                    }
                }
            });

            return;
        }


    }



}

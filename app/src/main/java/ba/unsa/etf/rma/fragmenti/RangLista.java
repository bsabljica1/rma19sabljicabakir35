package ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.ArrayMap;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.json.Json;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import ba.unsa.etf.rma.BazaDBOpenHelper;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;


public class RangLista extends Fragment {

    private String imeIgraca;
    private ArrayList<String> rangLista = new ArrayList<>();
    private double procenat;
    private String nazivKviza;
    private String dokumentRangliste;
    private ArrayList<String> ucitaniIgraciIme = new ArrayList<>();
    private ArrayList<Double> ucitaniIgraciProcenat = new ArrayList<>();
    private boolean novaLista = true;
    private BazaDBOpenHelper bazaDBOpenHelper;



    public class KreirajRangListTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                String url ="";
                HttpURLConnection conn;
                if (dokumentRangliste.length()>0) {
                    url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste/" + dokumentRangliste + "?access_token=";
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                    conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("PATCH");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                }
                else {
                    url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste?access_token=";
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                    conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                }

                String dokument = "{ \"fields\": { \"nazivKviza\": {\"stringValue\":\""+ nazivKviza +"\"}, \"lista\": {\"mapValue\": {\"fields\": {";
                String igraci="";
                for (int i=0; i<ucitaniIgraciIme.size(); i++) {
                    int pozicija = i+1;
                    igraci += "\""+pozicija+"\": {\"mapValue\": {\"fields\": {\""+ucitaniIgraciIme.get(i)+"\": {\"stringValue\":\""+ucitaniIgraciProcenat.get(i).intValue()+"\"}}}}";
                    if (i!=rangLista.size()-1) igraci+=", ";
                }
                dokument+=igraci+"}}}}}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim());
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }



    public class ProcitajBazuRangliste extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            int pomNbr = 1;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivKviza1 = konkretnoPitanje.getJSONObject("nazivKviza");
                            String n = nazivKviza1.getString("stringValue");
                            if (!n.equals(nazivKviza)) continue;
                            dokumentRangliste = dijelovi[6];
                            JSONObject lista = konkretnoPitanje.getJSONObject("lista");
                            JSONObject mapa = lista.getJSONObject("mapValue");
                            JSONObject pozicijeListaDva = mapa.getJSONObject("fields");

                            String pom = Integer.toString(pomNbr);

                            while (pozicijeListaDva.has(pom)) {
                                JSONObject map = pozicijeListaDva.getJSONObject(pom);
                                JSONObject fields = map.getJSONObject("mapValue");
                                String ime = fields.getString("fields");
                                String[] dio = ime.split(":");
                                String name = "";
                                for (int j=2; j<dio[0].length()-1; j++) {
                                    name += dio[0].charAt(j);
                                }
                                String broj = "";

                                for (int j=0; j<dio[2].length(); j++) {
                                    if (dio[2].charAt(j)=='"') {
                                        do {
                                            j++;
                                            if (dio[2].charAt(j)!='"')
                                            broj += dio[2].charAt(j);
                                        } while (dio[2].charAt(j)!='"');
                                    }
                                }
                                broj.trim();


                                Double proc = Double.parseDouble(broj);

                                ucitaniIgraciIme.add(name);
                                ucitaniIgraciProcenat.add(proc);
                                pomNbr += 1;
                                pom = Integer.toString(pomNbr);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (ucitaniIgraciIme.size() > 1) {

                String[] pomImena = new String[ucitaniIgraciIme.size()];
                int[] pomProcenti = new int[ucitaniIgraciProcenat.size()];

                for (int i = 0; i < ucitaniIgraciProcenat.size(); i++) {
                    pomImena[i] = ucitaniIgraciIme.get(i);
                    pomProcenti[i] = ucitaniIgraciProcenat.get(i).intValue();
                    System.out.println("Igrac: " + ucitaniIgraciIme.get(i) + ", Procent: " + ucitaniIgraciProcenat.get(i).intValue());
                }

                for (int i = 0; i <= pomImena.length - 2; i++) {
                    int max = pomProcenti[i];
                    int pmax = i;
                    String max1 = pomImena[i];
                    for (int j = i + 1; j <= pomImena.length - 1; j++) {
                        if ( pomProcenti[j] > max) {
                            max = pomProcenti[j];
                            max1 = pomImena[j];
                            pmax = j;
                        }
                    }
                    pomProcenti[pmax] = pomProcenti[i];
                    pomImena[pmax] = pomImena[i];
                    pomProcenti[i] = max;
                    pomImena[i] = max1;
                }

                ucitaniIgraciIme.clear();
                ucitaniIgraciProcenat.clear();

                int pozicija = 1;
                for (int i = 0; i < pomImena.length; i++) {
                    ucitaniIgraciIme.add(pomImena[i]);
                    ucitaniIgraciProcenat.add((double) pomProcenti[i]);
                    String red = pozicija + ".  Naziv igraca:  " + ucitaniIgraciIme.get(i) + ", Procenat tacnih:  " + ucitaniIgraciProcenat.get(i).intValue() + " %";
                    rangLista.add(red);
                    pozicija++;
                }
            }
            else {
                String red = 1 + ".  Naziv igraca:  " + ucitaniIgraciIme.get(0) + ", Procenat tacnih:  " + ucitaniIgraciProcenat.get(0).intValue() + " %";
                rangLista.add(red);
            }



            final ListView lista = (ListView) getView().findViewById(R.id.listaRang);;
            ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, rangLista);
            lista.setAdapter(adapter);

            if (dokumentRangliste.length() == 0) {
                Random rand = new Random();
                int broj = rand.nextInt(1000000);
                dokumentRangliste = Integer.toString(broj);
                bazaDBOpenHelper.UnosRanglisti(nazivKviza, new ArrayList<Kviz>(), new ArrayList<String>(), Integer.toString(broj));
                bazaDBOpenHelper.UnosKonkretneListe(imeIgraca, procenat, Integer.toString(broj));
            }

            else {
                bazaDBOpenHelper.UnosKonkretneListe(imeIgraca, procenat, dokumentRangliste);
            }

            new KreirajRangListTask().execute("dsd");

        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rang_lista, container, false);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceBundle) {
        super.onActivityCreated(savedInstanceBundle);

        if (getArguments().containsKey("imeIgraca")) {

            imeIgraca = getArguments().getString("imeIgraca");
            procenat = getArguments().getDouble("procenatTacnih");
            nazivKviza = getArguments().getString("nazivKviza");
            dokumentRangliste = "";


            if (getArguments().getBoolean("konekcija")) {
                ucitaniIgraciIme.clear();
                ucitaniIgraciProcenat.clear();
                ucitaniIgraciIme.add(imeIgraca);
                ucitaniIgraciProcenat.add(procenat);
                bazaDBOpenHelper = new BazaDBOpenHelper(getContext(), BazaDBOpenHelper.DATABASE_NAME, null, BazaDBOpenHelper.DATABASE_VERSION);
                new ProcitajBazuRangliste().execute("dsd");
            }

            else {

                bazaDBOpenHelper = new BazaDBOpenHelper(getContext(), BazaDBOpenHelper.DATABASE_NAME, null, BazaDBOpenHelper.DATABASE_VERSION);
                bazaDBOpenHelper.UcitajRanglistu(nazivKviza, ucitaniIgraciIme, ucitaniIgraciProcenat);

                if (ucitaniIgraciProcenat.size() == 0)
                    novaLista = true;
                else
                    novaLista = false;

                ucitaniIgraciIme.add(imeIgraca);
                ucitaniIgraciProcenat.add(procenat);


                if (novaLista) {
                    Random rand = new Random();
                    int broj = rand.nextInt(1000000);
                    bazaDBOpenHelper.UnosRanglisti(nazivKviza, new ArrayList<Kviz>(), new ArrayList<String>(), Integer.toString(broj));
                    bazaDBOpenHelper.UnosKonkretneListe(imeIgraca, procenat, Integer.toString(broj));
                }

                else {
                    dokumentRangliste = bazaDBOpenHelper.DajDokumentRangliste(nazivKviza);
                    bazaDBOpenHelper.UnosKonkretneListe(imeIgraca, procenat, dokumentRangliste);
                }


                if (ucitaniIgraciIme.size() > 1) {

                    String[] pomImena = new String[ucitaniIgraciIme.size()];
                    int[] pomProcenti = new int[ucitaniIgraciProcenat.size()];

                    for (int i = 0; i < ucitaniIgraciProcenat.size(); i++) {
                        pomImena[i] = ucitaniIgraciIme.get(i);
                        pomProcenti[i] = ucitaniIgraciProcenat.get(i).intValue();
                        System.out.println("Igrac: " + ucitaniIgraciIme.get(i) + ", Procent: " + ucitaniIgraciProcenat.get(i).intValue());
                    }

                    for (int i = 0; i <= pomImena.length - 2; i++) {
                        int max = pomProcenti[i];
                        int pmax = i;
                        String max1 = pomImena[i];
                        for (int j = i + 1; j <= pomImena.length - 1; j++) {
                            if ( pomProcenti[j] > max) {
                                max = pomProcenti[j];
                                max1 = pomImena[j];
                                pmax = j;
                            }
                        }
                        pomProcenti[pmax] = pomProcenti[i];
                        pomImena[pmax] = pomImena[i];
                        pomProcenti[i] = max;
                        pomImena[i] = max1;
                    }

                    ucitaniIgraciIme.clear();
                    ucitaniIgraciProcenat.clear();

                    int pozicija = 1;
                    for (int i = 0; i < pomImena.length; i++) {
                        ucitaniIgraciIme.add(pomImena[i]);
                        ucitaniIgraciProcenat.add((double) pomProcenti[i]);
                        String red = pozicija + ".  Naziv igraca:  " + ucitaniIgraciIme.get(i) + ", Procenat tacnih:  " + ucitaniIgraciProcenat.get(i).intValue() + " %";
                        rangLista.add(red);
                        pozicija++;
                    }
                }
                else {
                    String red = 1 + ".  Naziv igraca:  " + ucitaniIgraciIme.get(0) + ", Procenat tacnih:  " + ucitaniIgraciProcenat.get(0).intValue() + " %";
                    rangLista.add(red);
                }



                final ListView lista = (ListView) getView().findViewById(R.id.listaRang);;
                ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, rangLista);
                lista.setAdapter(adapter);

            }

        }

        }

}

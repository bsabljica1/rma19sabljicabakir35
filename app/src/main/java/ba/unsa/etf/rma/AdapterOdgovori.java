package ba.unsa.etf.rma;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class AdapterOdgovori extends ArrayAdapter<String> {


    ArrayList<String> odgovori = new ArrayList<>();

    public AdapterOdgovori(Context context, int textViewResourceId, ArrayList<String> objects) {
        super(context, textViewResourceId, objects);
        odgovori = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.element_liste, null);
        ImageView imageView = (ImageView) v.findViewById(R.id.slike);
        TextView textView = (TextView) v.findViewById(R.id.textView);
        textView.setText(odgovori.get(position));
        imageView.setImageResource(R.drawable.plavikrugic);
        return v;

    }

}
package ba.unsa.etf.rma.fragmenti;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.BazaDBOpenHelper;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;


public class InformacijeFrag extends Fragment {

    private Kviz kviz;
    private String naziv = new String();
    private String brojTacnih = new String();
    private String brojPreostalih = new String();
    private String procenatTacnih = new String();
    private Button krajIgre;
    private int tacni = 0;
    private int ukupni = 0;
    private int preostali =0;
    private double procent = 0;
    private BazaDBOpenHelper bazaDBOpenHelper;
    private ArrayList<String> dokumentiRanglisti;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View iv = inflater.inflate(R.layout.informacije_frag, container, false);

        krajIgre = (Button) iv.findViewById(R.id.btnKraj);


        if (getArguments()!=null) {


            kviz = (Kviz) getArguments().getSerializable("kviz");
            tacni = getArguments().getInt("brojTacnih");
            ukupni = getArguments().getInt("ukupniBrojPitanja");
            preostali = getArguments().getInt("brojPreostalih");

            naziv = kviz.getNaziv();
            brojTacnih = Integer.toString(tacni);
            brojPreostalih = Integer.toString(preostali);
            if (ukupni!=0) procent =((double)tacni/(double)ukupni)*100;
            else procent = 0;

            procenatTacnih = Integer.toString((int) procent)+"%";


            TextView nazivKviza = (TextView) iv.findViewById(R.id.infNazivKviza);
            TextView brojTacnihPitanja = (TextView) iv.findViewById(R.id.infBrojTacnihPitanja);
            TextView brojPreostalihPitanja = (TextView) iv.findViewById(R.id.infBrojPreostalihPitanja);
            TextView procenatTacnihPitanja = (TextView) iv.findViewById(R.id.infProcenatTacni);


            nazivKviza.setText(naziv);
            brojTacnihPitanja.setText(brojTacnih);
            brojPreostalihPitanja.setText(brojPreostalih);
            procenatTacnihPitanja.setText(procenatTacnih);
        }

        krajIgre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent closeNewAlarm = new Intent(AlarmClock.ACTION_DISMISS_ALARM);
                closeNewAlarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                startActivity(closeNewAlarm);

                Intent povratak = new Intent(getContext(), KvizoviAkt.class);
                boolean promjena = false;
                if (ukupni>0 && preostali==0)
                    promjena=true;
                povratak.putExtra("promjena", promjena);

                bazaDBOpenHelper = new BazaDBOpenHelper(getContext(), BazaDBOpenHelper.DATABASE_NAME, null, BazaDBOpenHelper.DATABASE_VERSION);
                dokumentiRanglisti = new ArrayList<>();
                for (int i = 0; i < bazaDBOpenHelper.DajBrojRanglisti(); i++)
                    new AzurirajRangListTask().execute("dajBoze");

                getActivity().finish();
            }
        });




        return iv;
    }


    public class AzurirajRangListTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getContext().getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                String url ="";
                HttpURLConnection conn;
                ArrayList<String> imena = new ArrayList<>();
                ArrayList<Double> procenti = new ArrayList<>();
                String dokumentRangliste = bazaDBOpenHelper.AzirirajRanglistu(imena, procenti, dokumentiRanglisti);
                String nazivKviza = bazaDBOpenHelper.DajKvizRangliste(dokumentRangliste);
                dokumentiRanglisti.add(dokumentRangliste);

                url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste/" + dokumentRangliste + "?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("PATCH");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\": { \"nazivKviza\": {\"stringValue\":\""+ nazivKviza +"\"}, \"lista\": {\"mapValue\": {\"fields\": {";
                String igraci="";
                for (int i=0; i<imena.size(); i++) {
                    int pozicija = i+1;
                    igraci += "\""+pozicija+"\": {\"mapValue\": {\"fields\": {\""+imena.get(i)+"\": {\"stringValue\":\""+procenti.get(i).intValue()+"\"}}}}";
                    if (i!=imena.size()-1) igraci+=", ";
                }
                dokument+=igraci+"}}}}}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim());
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }






}

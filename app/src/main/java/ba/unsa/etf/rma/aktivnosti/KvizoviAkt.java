package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.util.DateTime;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import ba.unsa.etf.rma.AdapterKategorija;
import ba.unsa.etf.rma.AdapterKviz;
import ba.unsa.etf.rma.BazaDBOpenHelper;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.R;

public class KvizoviAkt extends AppCompatActivity implements ListaFrag.OnItemClick {


    Spinner spinner;
    ListView listView;
    AdapterKategorija adapter;
    AdapterKviz adapter2;
    ArrayList<Kategorija> kategorije= new ArrayList<>();
    ArrayList<Kviz> kvizovi= new ArrayList<>();
    ArrayList<Kviz> trenutniKvizovi= new ArrayList<>();
    Kategorija sveKategorije = new Kategorija("Svi","svi");
    Kviz dodajKviz = new Kviz("Dodaj kviz", null, null);
    Kategorija kategorija = new Kategorija("a", "0");
    private FragmentManager fragmentManager;
    private FrameLayout listaFrameLayout;
    private ListaFrag listaFrag;
    private DetailFrag detailFrag;
    private ArrayList<Pitanje> pitanja = new ArrayList<>();
    private ArrayList<String> dokumentPitanja = new ArrayList<>();
    private ArrayList<String> dokumentKategorija = new ArrayList<>();
    private ArrayList<String> dokumentKvizova = new ArrayList<>();
    private Context con = this;
    private Boolean horizontalnaPromjena = false;
    private int pozicijaPromjene = -1;
    private ArrayList<String> ucitaniIgraciIme = new ArrayList<>();
    private ArrayList<Double> ucitaniIgraciProcenat = new ArrayList<>();
    private ArrayList<String> dokumentiRanglisti = new ArrayList<>();
    private BazaDBOpenHelper bazaDBOpenHelper;
    private boolean pokretanje = true;
    private ArrayList<String> ucitaniIgraci = new ArrayList<>();
    private ArrayList<Double> ucitaniProcenti = new ArrayList<>();
    private boolean promjena = false;


    public class ProcitajBazuKategorija extends AsyncTask<String, Void, Void> {

        private Context con;
        private Kategorija proslijedjena;

        public ProcitajBazuKategorija(Context c, Kategorija p) {
            con = c;
            proslijedjena = p;
        }

        @Override
        protected Void doInBackground(String... strings) {
            GoogleCredential credentials;
            try {
                InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            JSONObject kata = dokumenti.getJSONObject(i);
                            String str = kata.getString("name");
                            String[] dijelovi = str.split("/");
                            String dokumentId = dijelovi[6];
                            JSONObject konkretnaKata = kata.getJSONObject("fields");
                            JSONObject nazivKate = konkretnaKata.getJSONObject("naziv");
                            JSONObject idKate = konkretnaKata.getJSONObject("idIkonice");
                            String n = nazivKate.getString("stringValue");
                            String id = idKate.getString("integerValue");
                            boolean postojiKategorija = false;
                            for (int j=0; j<kategorije.size(); j++) {
                                if (kategorije.get(j).getNaziv().equals(n) || kategorije.get(j).getId().equals(id))
                                    postojiKategorija = true;
                            }
                            if (!postojiKategorija) {
                                kategorije.add(new Kategorija(n, id));
                                dokumentKategorija.add(dokumentId);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (KvizoviAkt.this.findViewById(R.id.listPlace) == null)
            adapter.notifyDataSetChanged();

            new ProcitajBazuPitanja(con, proslijedjena).execute("dsd");

        }
    }




    public class ProcitajBazuPitanja extends AsyncTask<String, Void, Void> {

        private Context con;
        private Kategorija proslijedjenja;

        public ProcitajBazuPitanja(Context c, Kategorija p) {
            con = c;
            proslijedjenja = p;
        }

        @Override
        protected Void doInBackground(String... strings) {

        GoogleCredential credentials;
        try {
            InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();

            String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Pitanja?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) !=null ) {
                    response.append(responseLine.trim() + "\n");
                }
                String inputStream = response.toString();
                try {
                    JSONObject jo = new JSONObject(inputStream);
                    JSONArray dokumenti = jo.getJSONArray("documents");
                    for (int i=0; i< dokumenti.length(); i++) {
                        JSONObject jedno = dokumenti.getJSONObject(i);
                        String str = jedno.getString("name");
                        String[] dijelovi = str.split("/");
                        String dokumenPitanja = dijelovi[6];
                        JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                        JSONObject nazivPitanja = konkretnoPitanje.getJSONObject("naziv");
                        JSONObject indexTacnogPitanja = konkretnoPitanje.getJSONObject("indexTacnog");
                        JSONObject lOdg = konkretnoPitanje.getJSONObject("odgovori");
                        JSONObject lOdgovora = lOdg.getJSONObject("arrayValue");
                        JSONArray listaOdgovora = lOdgovora.getJSONArray("values");

                        ArrayList<String> odgovori = new ArrayList<>();
                        for (int j=0; j<listaOdgovora.length(); j++) {
                            JSONObject jedanOdg = listaOdgovora.getJSONObject(j);
                            String value = jedanOdg.getString("stringValue");
                            odgovori.add(value);
                        }

                        String n = nazivPitanja.getString("stringValue");
                        String index = indexTacnogPitanja.getString("integerValue");
                        boolean postojiPitanje = false;
                        for (int j=0; j<pitanja.size(); j++) {
                            if (pitanja.get(j).getNaziv().equals(n))
                                postojiPitanje = true;
                        }


                            if (!postojiPitanje) {
                                pitanja.add(new Pitanje(n, n, odgovori, odgovori.get(Integer.parseInt(index))));
                                dokumentPitanja.add(dokumenPitanja);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (KvizoviAkt.this.findViewById(R.id.listPlace) == null) {
                new ProcitajBazuKvizova(con, proslijedjenja).execute();
            }

            else {

                if (horizontalnaPromjena) {
                    new ProcitajBazuKvizova(con, kategorije.get(pozicijaPromjene)).execute("dsd");
                }
                else {
                    new ProcitajBazuKvizova(con, sveKategorije).execute("dsd");
                }
            }

        }
    }







    public class ProcitajBazuKvizova extends AsyncTask<String, Void, Void> {



        private Context con;
        private Kategorija trenutnaKategorija;

        public ProcitajBazuKvizova(Context c, Kategorija k) {
            con = c;
            trenutnaKategorija = k;
        }



        @Override
        protected Void doInBackground(String... strings) {
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = con.getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();

            String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Kvizovi?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) !=null ) {
                    response.append(responseLine.trim() + "\n");
                }
                String inputStream = response.toString();
                try {
                    JSONObject jo = new JSONObject(inputStream);
                    JSONArray dokumenti = jo.getJSONArray("documents");
                    for (int i=0; i< dokumenti.length(); i++) {
                        JSONObject jedno = dokumenti.getJSONObject(i);
                        String str = jedno.getString("name");
                        String[] dijelovi = str.split("/");
                        String dokumentKviza = dijelovi[6];
                        JSONObject konkretanKviz = jedno.getJSONObject("fields");
                        JSONObject nazivKviza = konkretanKviz.getJSONObject("naziv");
                        JSONObject idKat = konkretanKviz.getJSONObject("idKategorije");
                        JSONObject lPit = konkretanKviz.getJSONObject("pitanja");
                        JSONObject lPitanja = lPit.getJSONObject("arrayValue");
                        ArrayList<String> pitanjaS = new ArrayList<>();
                        if (lPitanja.has("values")) {
                            JSONArray listaPitanja = lPitanja.getJSONArray("values");


                            for (int j = 0; j < listaPitanja.length(); j++) {
                                JSONObject jedanOdg = listaPitanja.getJSONObject(j);
                                String value = jedanOdg.getString("stringValue");
                                pitanjaS.add(value); // LISTA ID PITANJA
                            }

                        }

                        String n = nazivKviza.getString("stringValue"); //NAZIV KVIZA
                        String idKatString = idKat.getString("stringValue"); // ID KATEGORIJE

                        ArrayList<Pitanje> potrebnaPitanja = new ArrayList<>();

                        for (int j=0; j<pitanjaS.size(); j++) {
                            for (int k=0; k<pitanja.size(); k++) {
                                if (pitanjaS.get(j).equals(dokumentPitanja.get(k))) {
                                    potrebnaPitanja.add(pitanja.get(k));
                                    break;
                                }
                            }
                        }

                        Kategorija potrebnaKategorija = new Kategorija("Svi","svi");

                        for (int j=0; j<dokumentKategorija.size(); j++) {
                            if (dokumentKategorija.get(j).equals(idKatString))
                                potrebnaKategorija = kategorije.get(j+1);
                        }


                        boolean postojiKviz = false;
                        for (int j=0; j<kvizovi.size(); j++) {
                            if (kvizovi.get(j).getNaziv().equals(n))
                                postojiKviz = true;
                        }
                        if (!postojiKviz) {
                            if (trenutnaKategorija.getNaziv().equals("Svi")) {
                                kvizovi.add(new Kviz(n, potrebnaPitanja, potrebnaKategorija));
                                dokumentKvizova.add(dokumentKviza);
                            }
                            else {
                                if (trenutnaKategorija.getNaziv().equals(potrebnaKategorija.getNaziv()) && trenutnaKategorija.getId().equals(potrebnaKategorija.getId())) {
                                    kvizovi.add(new Kviz(n, potrebnaPitanja, potrebnaKategorija));
                                    dokumentKvizova.add(dokumentKviza);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

            return null;
        }

        @Override
        protected void onPreExecute() {

            kvizovi.clear();
            dokumentKvizova.clear();

        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (!kvizovi.contains(dodajKviz))
            kvizovi.add(dodajKviz);
            if (KvizoviAkt.this.findViewById(R.id.listPlace) == null) {
                adapter2.notifyDataSetChanged();
            }

            else {

                if (listaFrag == null) {
                    listaFrag = new ListaFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("listaKategorija", kategorije);
                    listaFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();
                }

                if (detailFrag == null) {
                    detailFrag = new DetailFrag();
                    Bundle argumenti2 = new Bundle();
                    argumenti2.putSerializable("potrebniKvizovi", kvizovi);
                    argumenti2.putSerializable("kategorije", kategorije);
                    argumenti2.putSerializable("dokumentKviza", dokumentKvizova);
                    detailFrag.setArguments(argumenti2);
                    fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
                }

                if (horizontalnaPromjena) {
                    listaFrag = new ListaFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("listaKategorija", kategorije);
                    listaFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();


                    detailFrag = new DetailFrag();
                    Bundle argumenti2 = new Bundle();

                    argumenti2.putSerializable("potrebniKvizovi", kvizovi);
                    argumenti2.putSerializable("kategorije", kategorije);
                    argumenti2.putSerializable("dokumentKviza", dokumentKvizova);
                    detailFrag.setArguments(argumenti2);
                    fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
                }


            }

            if (pokretanje) {
                ArrayList<Kategorija> pom = new ArrayList<>();
                for (int i=0; i<kategorije.size(); i++) {
                    if (!kategorije.get(i).getNaziv().equals(sveKategorije.getNaziv()))
                        pom.add(kategorije.get(i));
                }

                ArrayList<Kviz> pom2 = new ArrayList<>();
                for (int i=0; i<kvizovi.size(); i++) {
                    if (!kvizovi.get(i).getNaziv().equals(dodajKviz.getNaziv()))
                        pom2.add(kvizovi.get(i));
                }

                bazaDBOpenHelper.UnosKategorija(pom, dokumentKategorija);
                bazaDBOpenHelper.UnosKvizova(pom2, dokumentKvizova, pom, dokumentKategorija);
                bazaDBOpenHelper.UnosPitanja(pitanja, dokumentPitanja, pom2, dokumentKvizova);
                ArrayList<String> odgovori = new ArrayList<>();
                for (int i=0; i< pitanja.size(); i++) {
                    bazaDBOpenHelper.UnosOdgovora(pitanja.get(i).getOdgovori(), pitanja.get(i), dokumentPitanja.get(i));
                }

                new ProcitajBazuRangliste().execute("dsd");
            }

            pokretanje=false;
        }
    }







    public class ProcitajBazuRangliste extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                String url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim() + "\n");
                    }
                    String inputStream = response.toString();
                    try {
                        JSONObject jo = new JSONObject(inputStream);
                        String dokumentRangliste = "";
                        JSONArray dokumenti = jo.getJSONArray("documents");
                        for (int i=0; i< dokumenti.length(); i++) {
                            ucitaniIgraciIme.clear();
                            ucitaniIgraciProcenat.clear();
                            JSONObject jedno = dokumenti.getJSONObject(i);
                            String str = jedno.getString("name");
                            String[] dijelovi = str.split("/");
                            JSONObject konkretnoPitanje = jedno.getJSONObject("fields");
                            JSONObject nazivKviza1 = konkretnoPitanje.getJSONObject("nazivKviza");
                            String n = nazivKviza1.getString("stringValue");
                            dokumentRangliste = dijelovi[6];
                            JSONObject lista = konkretnoPitanje.getJSONObject("lista");
                            JSONObject mapa = lista.getJSONObject("mapValue");
                            JSONObject pozicijeListaDva = mapa.getJSONObject("fields");
                            int pomNbr = 1;
                            String pom = Integer.toString(pomNbr);

                            while (pozicijeListaDva.has(pom)) {
                                JSONObject map = pozicijeListaDva.getJSONObject(pom);
                                JSONObject fields = map.getJSONObject("mapValue");
                                String ime = fields.getString("fields");
                                String[] dio = ime.split(":");
                                String name = "";
                                for (int j=2; j<dio[0].length()-1; j++) {
                                    name += dio[0].charAt(j);
                                }
                                String broj = "";

                                for (int j=0; j<dio[2].length(); j++) {
                                    if (dio[2].charAt(j)=='"') {
                                        do {
                                            j++;
                                            if (dio[2].charAt(j)!='"')
                                                broj += dio[2].charAt(j);
                                        } while (dio[2].charAt(j)!='"');
                                    }
                                }
                                broj.trim();


                                Double proc = Double.parseDouble(broj);

                                ucitaniIgraciIme.add(name);
                                ucitaniIgraciProcenat.add(proc);
                                pomNbr += 1;
                                pom = Integer.toString(pomNbr);

                                bazaDBOpenHelper.UnosKonkretneListe(name, proc, dokumentRangliste);

                            }
                            ArrayList<Kviz> pom2 = new ArrayList<>();
                            for (int j=0; j<kvizovi.size(); j++) {
                                if (!kvizovi.get(j).getNaziv().equals(dodajKviz.getNaziv()))
                                    pom2.add(kvizovi.get(j));
                            }

                            bazaDBOpenHelper.UnosRanglisti(n, pom2, dokumentKvizova, dokumentRangliste);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }
    }









    public class AzurirajRangListTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                String url ="";
                HttpURLConnection conn;
                ArrayList<String> imena = new ArrayList<>();
                ArrayList<Double> procenti = new ArrayList<>();
                String dokumentRangliste = bazaDBOpenHelper.AzirirajRanglistu(imena, procenti, dokumentiRanglisti);
                String nazivKviza = bazaDBOpenHelper.DajKvizRangliste(dokumentRangliste);
                dokumentiRanglisti.add(dokumentRangliste);

                System.out.println("Dokument rang liste koja se azurira: " + dokumentRangliste);

                    url = "https://firestore.googleapis.com/v1/projects/projekatspirala/databases/(default)/documents/Rangliste/" + dokumentRangliste + "?access_token=";
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                    conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("PATCH");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\": { \"nazivKviza\": {\"stringValue\":\""+ nazivKviza +"\"}, \"lista\": {\"mapValue\": {\"fields\": {";
                String igraci="";
                for (int i=0; i<imena.size(); i++) {
                    int pozicija = i+1;
                    igraci += "\""+pozicija+"\": {\"mapValue\": {\"fields\": {\""+imena.get(i)+"\": {\"stringValue\":\""+procenti.get(i).intValue()+"\"}}}}";
                    if (i!=imena.size()-1) igraci+=", ";
                }
                dokument+=igraci+"}}}}}";
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) !=null ) {
                        response.append(responseLine.trim());
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            deleteDatabase("mojaBaza.db");
            new ProcitajBazuKategorija(con, sveKategorije).execute("dsd");
        }
    }










    public boolean ProvjeraKalendara (int position) {

        int brojPitanja = kvizovi.get(position).getPitanja().size() / 2 + 1;
        if (kvizovi.get(position).getPitanja().size() % 2 != 0)
            brojPitanja++;
        int x = brojPitanja * 60000;


        String[] projection = new String[] { CalendarContract.Events.CALENDAR_ID, CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION, CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND, CalendarContract.Events.ALL_DAY, CalendarContract.Events.EVENT_LOCATION };

        Calendar startTime = Calendar.getInstance();
        long endTime = startTime.getTimeInMillis();
        endTime+= x;
        String selection = "(( " + CalendarContract.Events.DTSTART + " >= " + startTime.getTimeInMillis() + " ) AND ( " + CalendarContract.Events.DTEND + " <= " + endTime + " ))" +
                " OR (( " + CalendarContract.Events.DTSTART + " >= " + startTime.getTimeInMillis() + " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + endTime + " ))" +
                " OR (( " + CalendarContract.Events.DTEND + " >= " + startTime.getTimeInMillis() + " ) AND ( " + CalendarContract.Events.DTEND + " <= " + endTime + " ))" +
                " OR (( " + CalendarContract.Events.DTSTART + " <= " + startTime.getTimeInMillis() + " ) AND ( " + CalendarContract.Events.DTEND + " >= " + endTime + " ))";
        Cursor cursor = this.getBaseContext().getContentResolver().query( CalendarContract.Events.CONTENT_URI, projection, selection, null, null );

        if (cursor.moveToFirst()) {
            return false;
        }
        cursor.close();

        return true;
    }


    public boolean ProvjeraKonekcije() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else
            connected = false;

        return connected;
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.kvizovi_akt);
        horizontalnaPromjena = false;
        pozicijaPromjene = -1;
        pokretanje = true;

        con = this;
        kategorije.clear();
        pitanja.clear();
        dokumentPitanja.clear();
        dokumentKategorija.clear();
        dokumentKvizova.clear();
        kategorije.add(sveKategorije);
        kvizovi.clear();

        Toast.makeText(KvizoviAkt.this, "Ucitavanje aplikacije u toku", Toast.LENGTH_LONG).show();
        bazaDBOpenHelper = new BazaDBOpenHelper(con, BazaDBOpenHelper.DATABASE_NAME, null, BazaDBOpenHelper.DATABASE_VERSION);
        promjena = getIntent().getBooleanExtra("promjena", false);

        if (ProvjeraKonekcije()) {

            //if (promjena) {

                for (int i = 0; i < bazaDBOpenHelper.DajBrojRanglisti(); i++)
                    new AzurirajRangListTask().execute("dajBoze");
            //}

           // deleteDatabase("mojaBaza.db");
           // new ProcitajBazuKategorija(this, sveKategorije).execute("dsd");
        }

        else {
            ArrayList<Kategorija> pom = bazaDBOpenHelper.UcitajKategorije();
            for (int i=0; i<pom.size(); i++) {
                kategorije.add(pom.get(i));
            }
            dokumentKategorija = bazaDBOpenHelper.UcitajDokumenteKategorija();
            kvizovi = bazaDBOpenHelper.UcitajKvizove(kategorije, dokumentKategorija, kategorija, "Svi");
            dokumentKvizova = bazaDBOpenHelper.UcitajDokumenteKvizova(kategorija, "Svi");
            kvizovi.add(dodajKviz);
        }



        if (KvizoviAkt.this.findViewById(R.id.listPlace) == null) {

            listView = (ListView) findViewById(R.id.lvKvizovi);
            adapter2 = new AdapterKviz(this, R.layout.ikone, kvizovi);
            listView.setAdapter(adapter2);
            spinner = (Spinner) findViewById(R.id.spPostojeceKategorije);
            adapter = new AdapterKategorija(this, android.R.layout.simple_spinner_item, kategorije);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(0);


            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    kategorija = adapter.getItem(position);

                    if (ProvjeraKonekcije()) {
                        dokumentKvizova.clear();
                        kvizovi.clear();
                            new ProcitajBazuKategorija(con, kategorija).execute("dsd");
                            if (KvizoviAkt.this.findViewById(R.id.listPlace) == null) {
                                adapter2 = new AdapterKviz(con, R.layout.ikone, kvizovi);
                                listView.setAdapter(adapter2);
                            }
                    }

                   else {

                       kvizovi.clear();

                           if (kategorija.getNaziv().equals("Svi")) {
                               kvizovi = bazaDBOpenHelper.UcitajKvizove(kategorije, dokumentKategorija, kategorija, "Svi");
                               dokumentKvizova = bazaDBOpenHelper.UcitajDokumenteKvizova(kategorija, "Svi");
                           }
                           else {
                               kvizovi = bazaDBOpenHelper.UcitajKvizove(kategorije, dokumentKategorija, kategorija, dokumentKategorija.get(position - 1));
                               dokumentKvizova = bazaDBOpenHelper.UcitajDokumenteKvizova(kategorija, dokumentKategorija.get(position-1));
                           }

                           kvizovi.add(dodajKviz);
                        if (KvizoviAkt.this.findViewById(R.id.listPlace) == null) {
                            adapter2 = new AdapterKviz(con, R.layout.ikone, kvizovi);
                            listView.setAdapter(adapter2);
                        }
                    }


                    adapter.notifyDataSetChanged();
                    adapter2.notifyDataSetChanged();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    if (!ProvjeraKonekcije()) {
                        Toast.makeText(KvizoviAkt.this, "Ne mozete azurirati kvizove bez pristupa internetu.", Toast.LENGTH_LONG).show();
                        return false;
                    }

                    Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                    String azuriranje = "ne";

                    if (kvizovi.get(position) != dodajKviz) {
                        azuriranje = "da";
                        myIntent.putExtra("dokumentKviza", dokumentKvizova.get(position));

                    }

                    myIntent.putExtra("azuriranje", azuriranje);

                    KvizoviAkt.this.startActivity(myIntent);

                    return true;

                }
            });


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if (kvizovi.get(position) == dodajKviz) return;

                    if (!ProvjeraKalendara(position)) {
                        AlertDialog alertDialog = new AlertDialog.Builder(KvizoviAkt.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Imate dogadjaj u kalendaru!");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                        return;
                    }

                    Intent igranjeKviza = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                    igranjeKviza.putExtra("dokumentKviza", dokumentKvizova.get(position));
                    igranjeKviza.putExtra("kviz", kvizovi.get(position));
                    bazaDBOpenHelper.UcitajRanglistu(kvizovi.get(position).getNaziv(), ucitaniIgraci, ucitaniProcenti);
                    igranjeKviza.putExtra("ucitaniIgraci", ucitaniIgraci);
                    igranjeKviza.putExtra("ucitaniProcenti", ucitaniProcenti);
                    igranjeKviza.putExtra("promjena", promjena);
                    KvizoviAkt.this.startActivity(igranjeKviza);


                }
            });

        }


        else {

            kvizovi = bazaDBOpenHelper.UcitajKvizove(kategorije, dokumentKategorija, kategorija, "Svi");
            dokumentKvizova = bazaDBOpenHelper.UcitajDokumenteKvizova(kategorija, "Svi");
            kvizovi.add(dodajKviz);

            fragmentManager = getSupportFragmentManager();
            listaFrameLayout = (FrameLayout) findViewById(R.id.listPlace);
            listaFrag = (ListaFrag) fragmentManager.findFragmentById(R.id.listPlace);


            fragmentManager = getSupportFragmentManager();
            listaFrameLayout = (FrameLayout) findViewById(R.id.detailPlace);
            detailFrag = (DetailFrag) fragmentManager.findFragmentById(R.id.detailPlace);

            if (!ProvjeraKonekcije()) {
                if (listaFrag == null) {
                    listaFrag = new ListaFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("listaKategorija", kategorije);
                    listaFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();
                }

                if (detailFrag == null) {
                    detailFrag = new DetailFrag();
                    Bundle argumenti2 = new Bundle();
                    argumenti2.putSerializable("potrebniKvizovi", kvizovi);
                    argumenti2.putSerializable("kategorije", kategorije);
                    argumenti2.putSerializable("dokumentKviza", dokumentKvizova);
                    argumenti2.putSerializable("konekcija", ProvjeraKonekcije());
                    detailFrag.setArguments(argumenti2);
                    fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
                }

                if (horizontalnaPromjena) {
                    listaFrag = new ListaFrag();
                    Bundle argumenti = new Bundle();
                    argumenti.putSerializable("listaKategorija", kategorije);
                    listaFrag.setArguments(argumenti);
                    fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();


                    detailFrag = new DetailFrag();
                    Bundle argumenti2 = new Bundle();

                    argumenti2.putSerializable("potrebniKvizovi", kvizovi);
                    argumenti2.putSerializable("kategorije", kategorije);
                    argumenti2.putSerializable("dokumentKviza", dokumentKvizova);
                    argumenti2.putSerializable("konekcija", ProvjeraKonekcije());
                    argumenti2.putSerializable("context",(Serializable) con);
                    detailFrag.setArguments(argumenti2);
                    fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
                }
            }

        }

    }


    public void onItemClicked(int pos) {

        horizontalnaPromjena = true;
        pozicijaPromjene = pos;

        kvizovi.clear();
        dokumentKvizova.clear();

        if (ProvjeraKonekcije())
        new ProcitajBazuKategorija(this, sveKategorije).execute("dsd");

        else {

            if (kategorije.get(pos).getNaziv().equals("Svi")) {
                kvizovi = bazaDBOpenHelper.UcitajKvizove(kategorije, dokumentKategorija, kategorije.get(pos), "Svi");
                dokumentKvizova = bazaDBOpenHelper.UcitajDokumenteKvizova(kategorije.get(pos), "Svi");
            }
            else {
                kvizovi = bazaDBOpenHelper.UcitajKvizove(kategorije, dokumentKategorija, kategorije.get(pos), dokumentKategorija.get(pos-1));
                dokumentKvizova = bazaDBOpenHelper.UcitajDokumenteKvizova(kategorije.get(pos), dokumentKategorija.get(pos-1));
            }

            kvizovi.add(dodajKviz);


            if (listaFrag == null) {
                listaFrag = new ListaFrag();
                Bundle argumenti = new Bundle();
                argumenti.putSerializable("listaKategorija", kategorije);
                listaFrag.setArguments(argumenti);
                fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();
            }

            if (detailFrag == null) {
                detailFrag = new DetailFrag();
                Bundle argumenti2 = new Bundle();
                argumenti2.putSerializable("potrebniKvizovi", kvizovi);
                argumenti2.putSerializable("kategorije", kategorije);
                argumenti2.putSerializable("dokumentKviza", dokumentKvizova);
                argumenti2.putSerializable("konekcija", ProvjeraKonekcije());
                argumenti2.putSerializable("context",(Serializable) con);
                detailFrag.setArguments(argumenti2);
                fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
            }

            if (horizontalnaPromjena) {
                listaFrag = new ListaFrag();
                Bundle argumenti = new Bundle();
                argumenti.putSerializable("listaKategorija", kategorije);
                listaFrag.setArguments(argumenti);
                fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();


                detailFrag = new DetailFrag();
                Bundle argumenti2 = new Bundle();

                argumenti2.putSerializable("potrebniKvizovi", kvizovi);
                argumenti2.putSerializable("kategorije", kategorije);
                argumenti2.putSerializable("dokumentKviza", dokumentKvizova);
                argumenti2.putSerializable("konekcija", ProvjeraKonekcije());
                detailFrag.setArguments(argumenti2);
                fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
            }

        }


    }


    }
